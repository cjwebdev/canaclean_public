<?php


$id = htmlspecialchars($_POST['id']);
$req = $bdd->prepare('SELECT * FROM products WHERE id = :id');
$req->execute(array(
  'id' => $id
));
if ( !($info = $req->fetch()) )
{
  header('Location: step2.php?error');
  exit;
}
$pdt = new Product($info);
$_SESSION['panier']->setProduct($pdt);

//Get all options for products
$opts = array();
$req = $bdd->prepare('SELECT * FROM options WHERE id_product = :id');
$req->execute(array(
  'id' => $id
));
while ( ($info = $req->fetch()) )
{
  array_push($opts, new Option($info));
}
$optsByLevel = array();
for ($i=0; $i < 10; $i++) {
  $optsByLevel[$i] = array();
  foreach ($opts as $opt) {
    if ($opt->getLevel() - 1 == $i){
      array_push($optsByLevel[$i], $opt);
    }
  }
}
?>
