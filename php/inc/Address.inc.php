<?php

 class Address{
  protected $_id;
  protected $_name;
  protected $_surName;
  protected $_numRoad;
  protected $_addr;
  protected $_addrComp;
  protected $_cp;
  protected $_fNumRoad;
  protected $_fAddr;
  protected $_fAddrComp;
  protected $_fCp;
  protected $_mail;
  protected $_tel;
  protected $_newsLetter;
  protected $_fVille;

  public function __construct($infotab)
  {
    $this->_surName = $infotab['surname'] ;
    $this->_name = $infotab['name'] ;
    $this->_numRoad = $infotab['numRoad'] ;
    $this->_addr = $infotab['addr'] ;
    $this->_addrComp = $infotab['addrComp'] ;
    $this->_cp = $infotab['cp'] ;
    $this->_fNumRoad = $infotab['fNumRoad'] ;
    $this->_fAddr = $infotab['fAddr'] ;
    $this->_fAddrComp = $infotab['fAddrComp'] ;
    $this->_fCp = $infotab['fCp'] ;
    $this->_fVille = $infotab['fVille'] ;
    $this->_mail = $infotab['mail'] ;
    $this->_tel = $infotab['tel'] ;
    $this->_newsLetter = $infotab['newsletter'] ;
  }

  public function getName(){ return $this->_name; }
  public function getSurName(){ return $this->_surName; }
  public function getNumRoad(){ return $this->_numRoad; }
  public function getAddr(){ return $this->_addr; }
  public function getAddrComp(){ return $this->_addrComp; }
  public function getCP(){ return $this->_cp; }
  public function getFNumRoad(){ return $this->_fNumRoad; }
  public function getFAddr(){ return $this->_fAddr; }
  public function getFAddrComp(){ return $this->_fAddrComp; }
  public function getFCP(){ return $this->_fCp; }
  public function getFVille(){ return $this->_fVille; }

  public function getMail(){ return $this->_mail; }
  public function getTel(){ return $this->_tel; }
  public function getNewsLetter(){ return $this->_newsLetter; }
  public function dumpTable()
  {
    ?>
    <tr>
      <td>Adresse :</td>
      <td><?php echo $this->_name . " " . $this->_surName; ?></td>
    </tr>
    <tr>
      <td></td>
      <td><?php echo $this->_numRoad . " " . $this->_addr; ?></td>
      <td></td>
    </tr>
    <?php
    if($this->_addrComp){
    ?>
    <tr>
      <td></td>
      <td><?php echo $this->_addrComp; ?></td>
      <td></td>
    </tr>
    <?php } ?>
    <tr>
      <td></td>
      <td>CP : <?php echo $this->_cp; ?></td>
      <td></td>
    </tr>
    <tr>
      <td>Adresse Facturation:</td>
      <td><?php echo $this->_fNumRoad . " " . $this->_fAddr; ?></td>
      <td></td>
    </tr>
    <?php
    if($this->_fAddrComp){
    ?>
    <tr>
      <td></td>
      <td><?php echo $this->_fAddrComp; ?></td>
      <td></td>
    </tr>
     <?php } ?>
    <tr>
      <td></td>
      <td>CP : <?php echo $this->_fCp; ?></td>
      <td></td>
    </tr>
    <?php
  }
  public function push(){ }

 };

?>
