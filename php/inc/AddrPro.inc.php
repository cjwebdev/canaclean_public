<?php

  include_once('Address.inc.php');
  class AddrPro extends Address{
    private $_nameEntp;
    private $_siret;
    private $_statut;
    public function __construct($infotab)
    {

      parent::__construct($infotab);
      $this->_nameEntp = $infotab['nameEntp'];
      $this->_siret = $infotab['siret'];
      $this->_statut = $infotab['statutEntp'];
    }
    public function getNameEntp(){ return $this->_nameEntp; }
    public function getSiret(){ return $this->_siret; }
    public function getStatut(){ return $this->_statut; }
    public function dump()
    {
      echo "<p>Construction :<br />" .
            "\t - name : " . $this->_name . "<br />" .
            "\t - surname : " . $this->_surName . "<br />" .
            "\t - numRoad : " . $this->_numRoad . "<br />" .
            "\t - addr : " . $this->_addr . "<br />" .
            "\t - addrComp : " . $this->_addrComp . "<br />" .
            "\t - cp : " . $this->_cp . "<br />" .
            "\t - mail : " . $this->_mail . "<br />" .

            "\t - newsletter : " . $this->_newsLetter . "<br />" .
            "\t - tel : " . $this->_tel .
            "</p>";
            /*
            $this->_surName = $surName ;
            $this->_name = $name ;
            $this->_numRoad = $numRoad ;
            $this->_addr = $addr ;
            $this->_addrComp = $addrComp ;
            $this->_cp = $cp ;
            $this->_fNumRoad = $fNumRoad ;
            $this->_fAddr = $fAddr ;
            $this->_fAddrComp = $fAddrComp ;
            $this->_fCodePostal = $fCodePostal ;
            $this->_mail = $mail ;
            $this->_tel = $tel ;
            $this->_newsLetter = $newsLetter ;
            */
    }
    public function addDb($db)
    {
      //, name, num_road, addr, addr_comp, cp, f_num_road, f_addr, f_addr_comp, f_cp, mail, tel, newsletter, locataire
      $req = $db->prepare('INSERT INTO addr_pro (surname, name, num_road, addr, addr_comp, cp, f_num_road, f_addr, f_addr_comp, f_cp, mail, tel, newsletter, statut, name_entp, siret)
                                          VALUES (:surname, :name, :num_road, :addr, :addr_comp, :cp, :f_num_road, :f_addr, :f_addr_comp, :f_cp, :mail, :tel, :newsletter, :statut, :name_entp, :siret)');
      $req->execute(array(
        'surname' => $this->_surName,
        'name' => $this->_name,
        'num_road' => $this->_numRoad,
        'addr' => $this->_addr,
        'addr_comp' => $this->_addrComp,
        'cp' => $this->_cp,
        'f_num_road' => $this->_fNumRoad,
        'f_addr' => $this->_fAddr,
        'f_addr_comp' => $this->_fAddrComp,
        'f_cp' => $this->_fCp,
        'mail' => $this->_mail,
        'tel' => $this->_tel,
        'newsletter' => $this->_newsLetter,
        'statut' => $this->_statut,
        'name_entp' => $this->_nameEntp,
        'siret' => $this->_siret
      ));
      return $db->lastInsertId();
    }
  };

?>
