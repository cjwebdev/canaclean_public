<?php

class Area{
  private $_id;
  private $_cp;
  private $_major;
  private $_name;
  private $_num_zone;

  public function __construct($infotab)
  {
    $this->_id = $infotab['id'];
    $this->_cp = $infotab['cp'];
    $this->_major = $infotab['major'];
    $this->_name = $infotab['name'];
    $this->_num_zone = $infotab['num_zone'];
  }
  public function getCP(){ return $this->_cp; }
  public function getMajor(){ return $this->_major; }
  public function getName(){ return $this->_name; }
  public function getId(){ return $this->_id; }
  public function getNumZone() { return $this->_num_zone; }

};

 ?>
