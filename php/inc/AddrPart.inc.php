<?php

  include_once('Address.inc.php');
  class AddrPart extends  Address{
    private $_locataire;

    public function __construct($infotab)
    {
      parent::__construct($infotab);
      $this->_locataire = $infotab['locataire'];
    }
    public function getLocataire(){ return $this->_locataire; }
    public function addDb($db)
    {
      //, name, num_road, addr, addr_comp, cp, f_num_road, f_addr, f_addr_comp, f_cp, mail, tel, newsletter, locataire
      $req = $db->prepare('INSERT INTO addr_part (surname, name, num_road, addr, addr_comp, cp, f_num_road, f_addr, f_addr_comp, f_cp, mail, tel, newsletter, locataire)
                                          VALUES (:surname, :name, :num_road, :addr, :addr_comp, :cp, :f_num_road, :f_addr, :f_addr_comp, :f_cp, :mail, :tel, :newsletter, :locataire)');
      $req->execute(array(
        'surname' => $this->_surName,
        'name' => $this->_name,
        'num_road' => $this->_numRoad,
        'addr' => $this->_addr,
        'addr_comp' => $this->_addrComp,
        'cp' => $this->_cp,
        'f_num_road' => $this->_fNumRoad,
        'f_addr' => $this->_fAddr,
        'f_addr_comp' => $this->_fAddrComp,
        'f_cp' => $this->_fCp,
        'mail' => $this->_mail,
        'tel' => $this->_tel,
        'newsletter' => $this->_newsLetter,
        'locataire' => $this->_locataire
      ));
      return $db->lastInsertId();
    }
    public function dumpTable()
    {
      parent::dumpTable();

      ?>
      <tr>
        <td></td>
        <td>Locataire</td>
        <?php if ($this->_locataire): ?>
          <td>Oui</td>
        <?php else: ?>
          <td>Non</td>
        <?php endif; ?>
      </tr>
      <?php
    }
  };

?>
