<?php
include_once($INCLUDE . 'Product.inc.php');
include_once($INCLUDE . 'Area.inc.php');
include_once($INCLUDE . 'AddrPro.inc.php');
include_once($INCLUDE . 'AddrPart.inc.php');
include_once($INCLUDE . 'Option.inc.php');

  class Panier{
    private $_product;
    private $_area;
    private $_address;
    private $_addrPro; //bool for taxe
    private $_options = array();
    private $_total_ttc;
    private $_total_ht;
    private $_commandId = -1;

    private function updateTotal()
    {
      if (isset($this->_product))
      {

        $price_ht = $this->_product->getPriceHT($this->_area->getNumZone());
        $this->_total_ttc = $price_ht;
        //Majoration des options de la prestations
        foreach ($this->_options as $opt) {
          if ($opt->inPercent())
            $this->_total_ttc += round((($opt->getMajor() / 100) * $price_ht), 2);
          else
            $this->_total_ttc += $opt->getMajor();
        }
        /*
        ** Prix differentiel pour Pro(20%) et particulier(10%)
        */
        $this->_total_ht = $this->_total_ttc;//Recup total no TVA
        if (isset($this->_addrPro))
        {
          if ($this->_addrPro)
            $this->_total_ttc += $this->_total_ttc / 100 * 20;
          else
            $this->_total_ttc += $this->_total_ttc / 100 * 10;
        }
        $this->_total_ttc = round($this->_total_ttc, 2);
      }
    }
    public function getTotalHT() { $this->updateTotal(); return $this->_total_ht; }
    public function getTotalTTC() { $this->updateTotal(); return $this->_total_ttc; }
    public function getTVA() { return ($this->_addrPro)?20:10; }

    public function isValid()
    {
      return isset($this->_produit) && isset($this->_area) && isset($this->_address);
    }
    public function getTotalZoneOptionsHT() {
      $price_ht = $this->_product->getPriceHT($this->_area->getNumZone());
      $res = $price_ht + (($this->_area->getMajor() / 100) * $price_ht);
      foreach ($this->_options as $opt) {
        if ($opt->inPercent())
          $res += round((($opt->getMajor() / 100) * $price_ht), 2);
        else
          $res += $opt->getMajor();
      }
      return $res;
    }
    public function setAddrType($n)
    {
      $this->_addrPro = $n;
      $this->updateTotal();
    }
    public function getAddrType()
    {
      return $this->_addrPro;
    }
    //Area
    public function setArea($area){
      $this->_area = $area;
      $this->updateTotal();
    }
    public function getArea(){ return $this->_area; }
    public function areaIsSet(){ return isset($this->_area); }

    //Product
    public function setProduct($pdt){
      $this->_product = $pdt;
      $this->updateTotal();
    }
    public function getProduct(){ return $this->_product; }
    public function productIsSet(){ return isset($this->_product); }

    //Address
    public function setAddress($addr){
      $this->_address = $addr;
      $this->updateTotal();
    }
    public function getAddress(){ return $this->_address; }
    public function IsSetAddress(){ return isset($this->_address);}

    public function addOption($nOpts){
      foreach ($this->_options as $opt) {
        if ($opt->getLevel() == $nOpts->getLevel())
        {
          $opt = $nOpt;
          return ;
        }
      }
      array_push($this->_options, $nOpts);
      $this->updateTotal();
    }
    public function getOptions(){
      return $this->_options;
    }
    public function dump()
    {
      //Calcul TVA
      $tva = ($this->_addrPro)?20:10;
      ?>
      <table>
        <tr>
          <th>Titre</th>
          <th>Intitulé</th>
          <th>Prix</th>
        </tr>
        <tr>
          <td>Prestation :</td>
          <td><?php echo $this->_product->getName(); ?></td>
          <td><?php echo $this->_product->getPriceHT($this->_area->getNumZone()); ?> € HT</td>
        </tr>
        <!-- Address -->
        <?php $this->_address->dumpTable(); ?>
        <?php $i = 0;foreach ($this->_options as $opt): ?>
          <tr>
            <?php if (!$i): ?>
              <td><?php if (!$i) echo "Options:"; ?></td>
            <?php else: ?>
              <td></td>
            <?php endif; ?>
            <td><?php echo $opt->getName(); ?></td>
            <td><?php echo $opt->getMajor(); ?> %</td>
          </tr>
        <?php $i++; endforeach; ?>
        <tr style="border-bottom:1px solid grey;">
          <td>Total HT :</td>
          <td></td>
          <td><?php echo $this->_total_ht; ?></td>
        </tr>
        <tr>
          <td>TVA</td>
          <td></td>
          <td><?php echo $tva; ?> %</td>
        </tr>
        <tr>
          <td>Total (TTC):</td>
          <td></td>
          <td><?php echo $this->_total_ttc; ?> €</td>
        </tr>
      </table>
      <?php
    }
    public function dumpRecap()
    {
      ?>
      <h2>Récapitulatif de commande</h2><br>
      <table>
        <tr>
          <td>Prestation</td>
          <td>Prix</td>
        </tr>
        <tr>
          <td><?php echo $this->_product->getName(); ?></td>
          <td><?php echo $this->getTotalZoneHT(); ?>€</td>
        </tr>
        <?php foreach ($this->_options as $opt): ?>
          <tr>
            <td><?php echo $opt->getName(); ?></td>
            <td><?php echo $opt->getMajor(); ?>€</td>
          </tr>
        <?php endforeach; ?>
        </table>
        <div style="float:right;width:35%;margin-right:15px;margin-top:35px">
          <table>
            <tr>
              <td>Prix HT</td>
              <td><?php echo $this->getTotalZoneHT(); ?>€</td>
            </tr>
            <tr>
              <td>TVA</td>
              <td><?php echo $this->getTVA(); ?>%</td>
            </tr>
            <tr>
              <td>Total</td>
              <td><?php echo $this->getTotalTTC(); ?>€</td>
            </tr>
          </table>
          <a href="facture.php" target="_blank" class="bouton">FACTURE</a>
          <a class="bouton">PAYER</a>
        </div>
      <?php
    }
    public function clean(){
      $this->_product = 0;
    }
    public function addDb($db)
    {
  
      $this->updateTotal();
      $addrId = $this->_address->addDb($db);
      $req = $db->prepare('INSERT INTO orderbase (date_send, id_addr, addr_pro, id_product, id_opt1, id_opt2, id_opt3, id_opt4, id_opt5, id_opt6, id_opt7, id_opt8, id_opt9, id_opt10, total_ttc, code_remise)
                                VALUES (NOW(), :id_addr, :addr_pro, :id_product, :id_opt1, :id_opt2, :id_opt3, :id_opt4, :id_opt5, :id_opt6, :id_opt7, :id_opt8, :id_opt9, :id_opt10, :total_ttc, :code_remise)');

      $opts = array();
      for ($i=1; $i < 11; $i++) {
        $opts[$i - 1] = 0;
        foreach ($this->_options as $one) {
          if ($one->getLevel() == $i)
            $opts[$i - 1] = $one->getId();
        }
      }
      $req->execute(array(
        'id_addr' => $addrId,
        'addr_pro' => $this->_addrPro,
        'id_product' => $this->_product->getId(),
        'id_opt1' => $opts[0],
        'id_opt2' => $opts[1],
        'id_opt3' => $opts[2],
        'id_opt4' => $opts[3],
        'id_opt5' => $opts[4],
        'id_opt6' => $opts[5],
        'id_opt7' => $opts[6],
        'id_opt8' => $opts[7],
        'id_opt9' => $opts[8],
        'id_opt10' => $opts[9],
        'total_ttc' => $this->_total_ttc,
        'code_remise' => 0
      ));
      $this->_commandId = $db->lastInsertId();
    }
    public function getIdCommandOnDb(){
      return $this->_commandId;
    }
    public function dumpImpaye(){
      ?>
      <h2>Récapitulatif de commande</h2><br>
      <table>
        <tr>
          <td>Prestation</td>
          <td>Prix</td>
        </tr>
        <tr>
          <td><?php echo $this->_product->getName(); ?></td>
          <td><?php echo $this->getTotalZoneHT(); ?>€</td>
        </tr>
        <?php foreach ($this->_options as $opt): ?>
          <tr>
            <td><?php echo $opt->getName(); ?></td>
            <td><?php echo $opt->getMajor(); ?>€</td>
          </tr>
        <?php endforeach; ?>
        </table>
        <div style="float:right;width:35%;margin-right:15px;margin-top:35px">
          <table>
            <tr>
              <td>Prix HT</td>
              <td><?php echo $this->getTotalZoneHT(); ?>€</td>
            </tr>
            <tr>
              <td>TVA</td>
              <td><?php echo $this->getTVA(); ?>%</td>
            </tr>
            <tr>
              <td>Total</td>
              <td><?php echo $this->getTotalTTC(); ?>€</td>
            </tr>
          </table>
          <a href="step1.php" class="bouton">RETOUR</a>
          <a class="bouton">IMPAYER</a>
        </div>
      <?php
    }
    public function getStep($step){
      ?>
      <center style="color:blue;text-align:left;" >
        <?php if ($step >= 1): ?>
          <a href="step1.php">Choix de la commune</a>
        <?php endif; ?>
        <?php if ($step >= 2): ?>
          >
          <a href="step2.php">Choix de la prestation</a>
        <?php endif; ?>
        <?php if ($step >= 3): ?>
        >
        <a href="step3.php">Choix de la prestation (2)</a>
        <?php endif; ?>
        <?php if ($step >= 4): ?>
        >
        <a href="step4.php">Addresse</a>
        <?php endif; ?>

      </center>
      <?php
    }
    /*
    public function set(){}
    public function get(){}
    public function IsSet(){}
    */

  };

?>
