<?php
  class Option{
    private $_id;
    private $_name;
    private $_majoration;
    private $_level;
    private $_id_pdt;
    private $_required;
    private $_desc;
    private $_percent;

    public function __construct($infotab)
    {
      $this->_id = $infotab['id'] ;
      $this->_name = $infotab['name'] ;
      $this->_majoration = $infotab['major'] ;
      $this->_level = $infotab['level'] ;
      $this->_id_pdt = $infotab['id_product'] ;
      $this->_required = $infotab['required'] ;
      $this->_desc = $infotab['description'];
      $this->_percent = $infotab['percent'];
    }
    public function getName() { return $this->_name; }
    public function getLevel() { return $this->_level; }
    public function getMajor() { return $this->_majoration; }
    public function getId(){ return $this->_id; }
    public function getRequired(){ return $this->_required; }
    public function getDesc(){ return $this->_desc; }
    public function inPercent(){ return $this->_percent; }
  };

?>
