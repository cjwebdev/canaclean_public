<?php
class Product{
  private $_id;
  private $_name;
  private $_price_ht = array();
  private $_desc;
  private $_img_link;


  public function __construct($infotab)
  {
    $this->_id = $infotab['id'];
    $this->_name = $infotab['name'];
    $this->_price_ht[0] = $infotab['price_ht'];
    $this->_price_ht[1] = $infotab['price_ht_1'];
    $this->_price_ht[2] = $infotab['price_ht_2'];
    $this->_price_ht[3] = $infotab['price_ht_3'];
    $this->_price_ht[4] = $infotab['price_ht_4'];
    $this->_price_ht[5] = $infotab['price_ht_5'];
    $this->_desc = $infotab['description'];
    $this->_img_link = $infotab['img_link'];
  }
  public function getId(){ return $this->_id; }
  public function getName(){ return $this->_name; }
  public function getPriceHT($num_zone){ return $this->_price_ht[$num_zone]; }
  public function getDesc(){ return $this->_desc; }
  public function getImgLink(){ return $this->_img_link; }


  public function getPricePlusOption($num_zone, $option)
  {
    $price_ht = $this->_price_ht[$num_zone];
    $major = $option->getMajor();
    $percent = $option->inPercent();

    if ($percent)
      return $price_ht + (($major / 100) * $price_ht);
    return $price_ht + $major;
  }


};


 ?>
