<!DOCTYPE html>
<html ⚡>
  <head>
    <title>Arcachon Débouchage canalisations</title>
 <meta charset="utf-8"><link rel="canonical" href="https://www.debouchage-bordeaux.fr/" />
	
	<meta name="description" content="Débouchage de canalisations à Arcachon par hydrocurage sans produits corrosifs sur RDV au 05 33 08 08 08. ">
		<?php   require('inc/meta.php') ?> 
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/FaviconMister-S.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><amp-img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"  width="370" height="217"  alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html ⚡>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
	          <?php $active = 'drain-cleaning';  require('inc/menuheader.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins2 mobile-center">
          <div class="container">
            <h2>Débouchage canalisations, tuyaux & drains d'épandage</h2>
            <div class="row off2">
			<div class="grid_4"><amp-img src="images/pipes.jpg"  width="370" height="217"  alt="Débouchage canalisations bordeaux"></amp-img>
                <h3>Débouchage canalisations</h3>
                <p>Des canalisations bouchées ou colmatée partiellement empêche les eaux usées de s'écouler normalement. Nos techniques de débouchage haute pression par hydrocurage restaurent la capacité d'écoulement dans les canalisations. Pour vous débarrasser de bouchons dans les canalisations, contactez-nous :)</p>
				<a href="<?php echo $url; ?>step1.php" >Découvrir</a>
				<a href="tel:001 0533080808" class="btpho btn  fa-phone">  Appel d'urgence</a>
              </div>
			<div class="grid_4"><amp-img src="images/canalisation-bouchee.jpg"    width="370" height="217"  alt="Tuyaux bouchés"></amp-img>
                <h3>Débouchage tuyauterie</h3>
                <p>Souvent, lorsque vos tuyaux "glougloutent" pendant l'évacuation des eaux, c'est un signe d'engorgement (bouchon) à venir. Un écoulement très lent = idem, vos tuyaux se bouchent. Pour remédier aux problèmes de tuyau bouché, contactez-nous. </p>
				<a href="<?php echo $url; ?>step1.php" >Découvrir</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'urgence</a>
              </div>
                 <div class="grid_4"><amp-img src="images/drain-sanitation.jpg"  width="370" height="217"  alt="drain d'épandage eaux usées"></amp-img>
                <h3>Débouchage drain d'épandage</h3>
                <p>L'écoulement des eaux traitées ne se fait pas correctement? Vos drains d'épandage sont peut être bouchés par des racines ou des matières non traitées par votre assainissement individuel. Pour un retour à la normale il est nécessaire d'hydrocurer les drains d'épandage.</p>
				<a href="<?php echo $url; ?>step1.php" >Découvrir</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'urgence</a>
              </div>
            </div>
		<hr>
            <h2>A Arcachon, Débouchage-Bordeaux.fr c'est aussi...</h2> 
			<div class="row">
              <div class="grid_4"><amp-img src="images/water-leak-detection.jpg"  width="370" height="217"  alt="Recherche de fuite"></amp-img>
                <h3>Recherche de Fuite</h3>
                <p>Les fuites d'eaux sont monaie courante dansun logement. Visible ou invisible, votre fuite d'eau s'aggravera avec le temps et c'est votre facture d'eau qui vous le prouvera.<br/>Pour éviter un dégât des eaux, mais aussi une note salée, contactez nous au plus vite.</p>
				<a href="recherche-fuite-eau.php" class="btn">+ d'infos</a>
              </div>
              <div class="grid_4"><amp-img src="images/camera-video-inspection.jpg"  width="370" height="217"  alt="Inspection des canalisations par caméra vidéo"></amp-img>
                <h3>Inspection vidéo des canalisations</h3>
                <p>Pour connaitre la cause de vos problèmes d'écoulement des eaux (lent ou inexistant), l'inspection des canalisations par vidéo caméra en est la solution. Racines invasives, effondrement de paroi, canalisations cassée, bouchon, colmatage, le passage caméra dans les canalisations apporte la preuve en vidéo. </p>
				<a href="inspection-video-canalisations.php" class="btn">+ d'infos</a>
              </div>
              <div class="grid_4"><amp-img src="images/hydrocurage-avant-apres.jpg"  width="370" height="217"  alt="Hydrocurage de canalisations bouchées"></amp-img>
                <h3>Hydrocurage Canalisations</h3>
                <p>Pour conserver des canalisations avec un diamètre d'écoulement originel, rien de tel qu'un hydrocurage. La tête hydrocureuse avance en disloquant les matières et nettoie les parois de vos canalisations. N'attendez pas d'avoir un problème de canalisation bouchée pour nous contacter.</p>
				<a href="hydrocurage-canalisations.php" class="btn">+ d'infos</a>
              </div>
            </div>
			<div class="row">
              <div class="grid_4"><amp-img src="images/hydrocurage-bacagraisse.jpg"  width="370" height="217"  alt="Vidange et nettoyage bac à graisse à Arcachon"></amp-img>
                <h3>Vidange de bac à graisse</h3>
                <p>Votre bac à graisse retient les graisses présentes dans les eaux usées de vos éviers, machine à laver le linge et lave vaisselle. Ce dispositf d'assainissement effectue le stockage des graisses pour rejeter une eaux claire soit dans votre système d'assainissement (une fosse septique) ou directement au tout à légout. Son entretien régulier permet d'éviter des bouchons graisseux dans les canalisations. Contactez-nous pour la vidange de votre bac à graisse.</p>
				<a href="merignac.php#myform" class="btn">Prendre rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'urgence</a>
              </div>
              <div class="grid_4"><amp-img src="images/raccordement-tout-egout.jpg"  width="370" height="217"  alt="Raccordement tout à l'égout, assainissement collectif de Arcachon"></amp-img>
                <h3>Raccordement tout à l'égout</h3>
                <p>Votre domicile est situé dans une zone collective rattachable au réseau public d'assainissement collectif. Vous devez obligatoire procéder au raccordement au tout à l'égout même si vous possédez une fosse septique. Cette démarche de raccordement doit etre effectuée dans un délai de 2 ans à compter de la mise en service du réseau d'assainissement collectif.  </p>
				<a href="merignac.php#myform" class="btn">Prendre rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'urgence</a>
              </div>
              <div class="grid_4"><amp-img src="images/vidange-fosse-septique.jpg"  width="370" height="217"  alt="Vidange d'une fosse septique à Arcachon"></amp-img>
                <h3>Vidange de fosse septique</h3>
                <p>Votre fosse septique ou fosse toutes eaux a pour objectif le pré-traitement des eaux usées et eaux vannes en retant les matières et les graisses. Au fil du temps cette couche de matière s'épaissit et ne rempli plus sa fonction d'assainissement individuel en rejetant des matières organiques polluantes dans l'environnement. C'est pourquoi il est indispensable et obligatoire de procéder à la vidange de votre fosse septique environ tous les 4 ans. </p>
				<a href="merignac.php#myform" class="btn">Prendre rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'urgence</a>
              </div>
            </div>			
          </div>
        </section>
        <section class="well1 ins4 bg-image-drainpipe">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Débouchage canalisations, tuyaux & drains d'épandage</h2>
                <p>En contactant "débouchage canalisations Bordeaux" vous faites appel à une équipe expérimentée et spécialisée dans le nettoyage  par hydrocurage des canalisations bouchées de canalisations. Avec nos camions hydrocureurs nous déboucheront vos canalisations, c'est promis...</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>Débouchage canalisations</li>
                      <li>débouchage tuyauterie</li>					  
                      <li>Débouchage drain d'épandage</li>
                      <li>Débouchage canalisations d'assainissement</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>Hydrocurage canalisations</li>
                      <li>Détartrage canalisations</li>
                      <li>Désembouage canalisations</li>
                      <li>Nettoyage canalisations</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="well1">
          <div class="container">
            <h2 class="mobile-center" id="tarifs-debouchage">Nos Tarifs</h2>
            <div class="row">
              <div class="grid_4">
                <table class="wow fadeInUp">
                  <tr>
                    <td>Débouchage Manuel</td>
                    <td>à partir de 99€ HT </td>
                  </tr>
                  <tr>
                    <td>Débouchage Haute Pression</td>
                    <td>249€ HT</td>
                  </tr> 
				  <tr>
                    <td>Hydrocurage</td>
                    <td> à partir de 249€ HT</td>
                  </tr>
                  <tr>
                    <td>Inspection Vidéo</td>
                    <td>249€ HT </td>
                  </tr>
                  <tr>
                    <td>Recherche de Fuite</td>
                    <td>249€ HT</td>
                  </tr>
                </table>
              </div>

              <div class="grid_4">
<ul>
<li>Hors taxes:
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> particuliers = +10%,</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> professionnels = +20%</li></li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Notre prestation de "Débouchage manuel" / "débouchage domestique" comprend le débouchage d’un évier, siphon, bonde à l'aide d'une pompe manuelle ou d'un furet.  Le débouchage domestique ne comprends pas les prestations de curage et/ou hydrocurage des canalisations.</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Obligation du client : <br/>
Lors d’une prestation en hydrocurage : Mettre à disposition de notre technicien une arrivée d’eau.</li>
</ul>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html ⚡>