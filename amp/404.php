<!DOCTYPE html>
<html ⚡>
  <head>
    <title>Erreur 404</title>
 <meta charset="utf-8"><link rel="canonical" href="https://www.debouchage-bordeaux.fr/" />
	
	<meta name="description" content="Erreur 404 ; www.debouchage-bordeaux.fr : Cette page n'existe pas ou n'est plus disponible">
	<meta nae="keyxord" content="débouchage, débouchage wc, débouchage canalisations, déboucher canalisations, débouchage les wc, débocher ma baignoire, déboucher mes toilettes">
	<?php   require('inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/FaviconMister-S.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><amp-img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html ⚡>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
          <?php require('inc/menuheader.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
 
        <section class="well1 ins4 bg-image-fuite-eau">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Erreur 404 : OOooops, Nous aussi on peut être victime de fuite :( </h2>
                <p>Page non trouvée ou inexistante. Essayer encore ou suivez un des liens présents ci dessous. Visitez notre site à cette adresse : <a href="https://www.debouchage-bordeaux.fr" >https://www.debouchage-bordeaux.fr</a></p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="marked-list wow fadeInRight">
                      <li><a href="https://www.debouchage-bordeaux.fr">Retournez à l'accueil de débouchage-bordeaux.fr</a></li>
                      <li><a href="https://www.debouchage-bordeaux.fr/wc.php">Débouchage WC</li>
                      <li><a href="https://www.debouchage-bordeaux.fr/canalisations.php">Débouchage Canalisations</li>					  
                      <li><a href="https://www.debouchage-bordeaux.fr/hydrocurage-canalisations.php">Hydrocurage canalisations</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li><a href="https://www.debouchage-bordeaux.fr/inspection-video-canalisations.php">Passage Caméra dans les canalisations</li>
                      <li><a href="https://www.debouchage-bordeaux.fr/recherche-fuite-eau.php">Recherche de fuites</li>
                      <li><a href="https://www.debouchage-bordeaux.fr/contacts.php#tarifs-debouchage">Nos tarifs</li>
                      <li><a href="https://www.debouchage-bordeaux.fr/contacts.php">Restons en contact</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html ⚡>