<!DOCTYPE html>
<html ⚡>
  <head>
    <title>Arcachon : accueil sur rendez-vous</title>
 <meta charset="utf-8"><link rel="canonical" href="https://www.debouchage-bordeaux.fr/" />
	
	<meta name="description" content="Débouchage Bordeaux, Permanence de Arcachon : Accueil sur Rendez vous">
	<meta name="keyword" content="débouchage arcachon, débouchage wc arcachon, débouchage canalisations arcachon, déboucher wc arcachon, société débouchage arcachon, plombier débouchage arcacon">
	<?php   require('inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/FaviconMister-S.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/google-map.css">
    <link rel="stylesheet" href="css/mailform.css">	
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><amp-img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html ⚡>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
          <?php require('inc/menuheader.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
 	  		  <?php
if (isset($_POST) && isset($_POST['nom']) && isset($_POST['telephone']) && isset($_POST['email']) && isset($_POST['message']))
{
    include("inc/checkform.php");
}
?>
        <section class="well1 ins4 bg-image-fuite-eau">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Débouchage Bordeaux : Permanence de Arcachon</h2>
                <p>Pour de multiples raisons, vous souhaitez nous rencontrer dans notre permanence de Mérinac : Vous rencontrerez notre commercial ou nos chargés d'études pour votre projet assainissement, canalisations, contrat d'entretien ...</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="marked-list wow fadeInRight">Débouchage Bordeaux à Arcachon c'est toujours...
                      <li><a href="https://www.debouchage-bordeaux.fr/wc.php" title="Débouchage WC à Arcachon">Débouchage WC</li>
                      <li><a href="debouchagecanalisations-arcachon.php" title="Débouchage canalisations, dégorgement sur rendez vous ou en urgence">Débouchage Canalisations</li>		  
                      <li><a href="https://www.debouchage-bordeaux.fr/hydrocurage-canalisations.php">Hydrocurage canalisations</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li><a href="https://www.debouchage-bordeaux.fr/inspection-video-canalisations.php">Passage Caméra dans les canalisations</li>
                      <li><a href="https://www.debouchage-bordeaux.fr/recherche-fuite-eau.php">Recherche de fuites</li>
                      <li><a href="arcachon.php#tarifs-debouchage">Nos tarifs</li>
					</ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
		</section>

         <section class="well1">
          <div class="container">
		<div class="row">
              <div class="grid_4 ">
                <div class="info-box">
                  <a href="tel:0630061182"><h2 ><i class="fa-phone"></i> 24/7 Débouchage d'urgence au <br/> 06 30 06 11 82</h2></a>
                  <hr>
				   <a href="tel:0533080808"><h3 ><i class="fa-fax"></i> Prendre un rendez-vous</h3><h3>   05 33 08 08 08</h3></a>
				  
                </div>
<h2>Services Débouchage</h2>
                <p>Vous rencontrez des problèmes de wc bouché, de canalisations bouchées, obstruées ou colmatées. Nous intervenons rapidement tous les jours de la semaine, en soirée, weekends et jours fériés</p>
                <ul class="marked-list">
                  <li><a href="wc.php">Débouchage WC</a></li>
                  <li><a href="canalisations.php">Débouchage canalisations</a></li>
                  <li><a href="hydrocurage-canalisations.php">Hydrocurage canalisations</a></li>
                  <li><a href="recherche-fuite-eau.php">Recherche de fuite</a></li>
				  <li><a href="inspection-video-canalisations.php">Inspection vidéo caméra des canalisations</a></li>

                </ul>
          </div>		
		<div class="grid_4">

            <h2 id="myform">Devis, Rendez-vous, Information</h2>
		   <form method="post" action="contacts.php"  class="mailform off2">
				
				<p>Laissez nous votre message, nous vous répondrons dans les plus brefs délais<br/><i>Tous les champs marqués par un * sont obligatoires</i></p>
              <fieldset class="row">
                <label class="grid_4" for="name">
                  <input type="text"  placeholder="Votre NOM : *"  name="nom"  >
                </label>
                <label class="grid_4" for="telephone">
                  <input type="text"  placeholder="Telephone : *" name="telephone" >
                </label>
                <label class="grid_4" for="email">
                  <input type="text"  placeholder="Email : *" name="email">
                </label>
                <label class="grid_4" for="message">
                  <textarea  placeholder="Merci de détailler votre demande : *" name="message"></textarea>
                </label>
                <div class="mfControls grid_4">
                  <button type="submit" class="btn">Envoyer</button>
                </div>
              </fieldset>
            </form>
			</div>
              <div class="grid_4">
                <div class="info-box">
                  <h2 class="fa-comment">Restons en contact !</h2>
                  <hr>
                  <h3>Posez vos questions à un professionnel du débouchage et des canalisations</h3>
				  <p>J'ai Besoin </p>
				<ul>
				<li><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> d'un <strong>devis</strong> pour un <strong>débouchage</strong></li>
				<li><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> d'un <strong>rendez-vous</strong> rapide pour une <strong>urgence débouchage</strong></li>
				<li><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> d'une <strong>information</strong> pour un <strong>projet de plomberie/assainissement</strong></li></ul>
                 			  
                  <hr>
                  <dl>
                    <h3><i class="fa fa-clock-o" aria-hidden="true"></i> Horaires</h3>
					<dt><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Lundi - Samedi : 07h00 - 23h00</dt>
					<dt><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Dimanche : OUVERT</dt>
					<dt><i class="fa fa-ellipsis-h" aria-hidden="true"></i> SOIRÉE : OUVERT</dt>
					<dt><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Jours Fériés : OUVERT</dt>
                  </dl>
                  <dl>
                    <dt><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:contact@ag-assainissement.fr">contact@ag-assainissement.fr</a></dt>
                  </dl>
                </div>
 
          </div>			
			</div>
          </div>
	        </section>	
			        <section class="well1">
          <div class="container">
            <h2 class="mobile-center" id="tarifs-debouchage">Nos Tarifs</h2>
            <div class="row">
              <div class="grid_4">
                <table class="wow fadeInUp">
                  <tr>
                    <td>Débouchage Manuel</td>
                    <td>à partir de 99€ HT </td>
                  </tr>
                  <tr>
                    <td>Débouchage Haute Pression</td>
                    <td>249€ HT</td>
                  </tr> 
				  <tr>
                    <td>Hydrocurage</td>
                    <td> à partir de 249€ HT</td>
                  </tr>
                  <tr>
                    <td>Inspection Vidéo</td>
                    <td>249€ HT </td>
                  </tr>
                  <tr>
                    <td>Recherche de Fuite</td>
                    <td>249€ HT</td>
                  </tr>
                </table>
              </div>

              <div class="grid_4">
<ul>
<li>Hors taxes:
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> particuliers = +10%,</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> professionnels = +20%</li></li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Notre prestation de "Débouchage manuel" / "débouchage domestique" comprend le débouchage d’un évier, siphon, bonde à l'aide d'une pompe manuelle ou d'un furet.  Le débouchage domestique ne comprends pas les prestations de curage et/ou hydrocurage des canalisations.</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Obligation du client : <br/>
Lors d’une prestation en hydrocurage : Mettre à disposition de notre technicien une arrivée d’eau.</li>
</ul>
              </div>
			  <div class="grid_4">
			  <?php include('inc/ville-desservies.php')?> 
			  </div>
            </div>
          </div>
        </section>			
       <section class="map">
          <div id="google-map" class="map_model"></div>
          <ul class="map_locations">
            <li data-x="-0.5288424" data-y="44.8220516">
              <p>31 rue Aristide Bergès, 33270 FLOIRAC<span> - 05 33 08 08 08</span></p>
            </li>
			<li data-x="-0.246095" data-y="44.554716">
              <p>Permanence de LANGON - 7 rue des Docteurs Théry, 33210 LANGON</p>
            </li>
			<li data-x="-0.24957" data-y="44.9133456">
              <p>Permanence de LIBOURNE - 7 rue Jules Ferry, 33500 LIBOURNE</p>
            </li>
			<li data-x="-0.6245137" data-y="44.83121">
              <p>Permanence de MERIGNAC - 97 Rue Paul Doumer, 33700 MERIGNAC</p>
            </li>
			<li data-x="-0.5785653" data-y="44.8200056">
              <p>Permanence de BORDEAUX NANSOUTY - 85 Rue de Marmande, 33800 Bordeaux</p>
            </li>
			<li data-x="-1.1569225" data-y="44.6389544">
              <p>Permanence d'ARCACHON - Bâtiment C, 47 Rue Lagrua, 33260 La Teste-de-Buch, </p>
            </li>			
			<li data-x="-0.7666923" data-y="45.213569">
              <p>Permanence de PAUILLAC - 12 Route de Bordeaux, 33250 Pauillac</p>
            </li>
			</ul>
        </section>		
      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html ⚡>