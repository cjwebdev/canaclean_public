<!DOCTYPE html>
<html ⚡>
  <head>
    <title>Mentions légales</title>
 <meta charset="utf-8"><link rel="canonical" href="https://www.debouchage-bordeaux.fr/" />
	
	<meta name="description" content="Mntions légales 'www.debouchage-bordeaux.fr'">
	<?php   require('inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/FaviconMister-S.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/google-map.css">
    <link rel="stylesheet" href="css/mailform.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><amp-img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"  width="370" height="217"  alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html ⚡>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
       <header>
        <?php require('inc/menuheader.php')?> 
      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1">
      <div class="container">
        <h4 class="coltext1">mentions légales</h4>
        <dl class="term-list">
          <dt>Hébergement & conception</dt> 
          <dd>
             <ul>
			 Ce site est la propriété d‘AG Assainissement.
			 <li>Siège : 31 rue Aristide Bergès, 33270 FLOIRAC</li>
			 <li>Directeur de la publication : Jimmy BOUBLI, dirigeant</li>
			 <li>Réalisation-Développement : JULU</li>
			 <li>Mise à jour : JULU</li>
			 <li>Hébergement : OVH</li>
			 </ul>

			 
          </dd> 
          <dt>Propriété intellectuelle</dt> 
          <dd>
            
La structure générale, ainsi que les textes, sons, graphismes, documents téléchargeables, bases de données et tout autre élément composant le site sont la propriété exclusive d’AG ASSAINISSEMENT.
L’ensemble de ce site relève de la législation française et internationale sur les droits d’auteur et de la propriété intellectuelle. Toute reproduction, totale ou partielle, du site, des éléments qui le composent et/ou des informations qui y figurent, par quelque procédé que ce soit, constitue une contrefaçon sanctionnée par le Code de la propriété intellectuelle.
Les marques AG ASSAINISSEMENT et de ses partenaires, ainsi que les logos figurant sur le site sont des marques déposées.
Toutefois, les informations propres à ce site peuvent être utilisées par des tiers dans les conditions suivantes après accord écrit d’AG ASSAINISSEMENT et à condition d’en mentionner la source. Elles ne pourront toutefois être utilisées à des fins commerciales ni publicitaires. AG ASSAINISSEMENT se réserve la possibilité, à tout moment, sans préavis et sans avoir à motiver sa décision, d’interdire aux tiers concernés l’utilisation des informations telles que définies ci-avant.
Tout utilisateur ou visiteur du site web ne peut mettre en place un hyperlien en direction de ce site sans l’autorisation expresse et préalable d’AG ASSAINISSEMENT.
          </dd>
          <dt>Déclaration CNIL</dt> 
          <dd>
            
En conformité avec les dispositions de la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, le traitement automatisé des données nominatives réalisé à partir du site font l’objet d’une déclaration auprès de la Commission Nationale de l’Informatique et des Libertés (CNIL).
Aucune utilisation commerciale des informations sur les entreprises ou sur les personnes n’est faite par AG ASSAINISSEMENT. Conformément aux articles 39 et suivants de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, toute personne peut obtenir communication et, le cas échéant, rectification ou suppression des informations la concernant, en s’adressant par courrier à AG ASSAINISSEMENT ou par mail à contact@ag-assainissement.fr
          </dd>
          <dt>Contenus</dt> 
          <dd>
          
Ce site met à la disposition des internautes des informations sur AG ASSAINISSEMENT et ses prestations. Les informations communiquées sont nécessairement partielles.
AG ASSAINISSEMENT ne peut garantir l’absence de modification du site par un tiers (intrusion, virus…) et décline toute responsabilité quant au contenu des présentes pages et à l’utilisation qui pourrait en être faite par quiconque.
AG ASSAINISSEMENT décline toute responsabilité quant aux défauts ou dommages qui pourraient survenir par la suite de la consultation ou de l’utilisation de ce site.
          </dd>
          <dt></dt> 
          <dd>
           
          </dd>
          <dt></dt> 
          <dd>
           
          </dd>
          <dd><a href="mailto:contact@ag-assainissement.fr">contact@ag-assainissement.fr</a></dd>
        </dl>
      </div>
        </section>
      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 
      </footer>
    </div>
	<script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html ⚡>