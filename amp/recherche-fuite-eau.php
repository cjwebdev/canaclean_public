<!DOCTYPE html>
<html ⚡>
  <head>
    <title>Recherche de fuite</title>
 <meta charset="utf-8"><link rel="canonical" href="https://www.debouchage-bordeaux.fr/" />
	
	<meta name="description" content="Recherche de fuite non destructive par caméra themique, gaz traceur ou détecteur acoustique. Evitez une facture trop salée en réagissant des aujourd'hui">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
		<?php   require('inc/meta.php') ?> 
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/FaviconMister-S.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><amp-img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"  width="370" height="217"  alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html ⚡>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
          <?php $active = 'water-leak-detection'; require('inc/menuheader.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins2 mobile-center">
          <div class="container">
            <h2>Recherche de fuite & réparation</h2>
            <div class="row off2">
			<div class="grid_4"><amp-img src="images/help-leak-inmyhome.jpg"  width="370" height="217"  alt=""></amp-img>
                <h3>Au secours, j'ai une fuite d'eau</h3>
                <p>Un robinet qui goutte = 120 litres d'eau perdue par jour.<br/> Une fuite d'eau (un simple filet continu) = 600L d'eau perdue par jour, soit l'équivalent de la consommation d'eau quotidenne d'une famille de 4 personnes. Bref une fuite d'eau peu vous couter très cher. Alors prêt pour nous appeler pour la détection de fuite d'eau et sa réparation rapide?
				</p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone"> Appel d'Urgence</a>
              </div>
              <div class="grid_4"><amp-img src="images/water-leak-detection.jpg"  width="370" height="217"  alt="water leak detection with a thermal camera "></amp-img></amp-img>
                <h3>Recherche de fuite</h3>
                <p>Les fuites d'eau sont toujours un problème. De simple goutte à filet d'eau, trouver l'emplacement exacte de la fuite reste primordial pour éviter une facture d'eau salée mais aussi un dégât des eaux pouvant dégrader votre habitation et vos voisins si vous êtes en immeuble. Votre objectif limiter les dégâts et rapidement. Pour ce faire , contactez-nous rapidement.</p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone"> Appel d'Urgence</a>
              </div>


                 <div class="grid_4"><amp-img src="images/water-leak-pipes.jpg"  width="370" height="217"  alt=""></amp-img>
                <h3>Coment savoir si j'ai réélement une fuite d'eau sur mon réseau? </h3>
                <p>Le soir avant de vous coucher
Fermer tous les robinets de votre habitation (machine à laver, lave vaisselle compris)
Relever votre compteur d’eau
Le lendemain matin :
Relever à nouveau votre compteur d’eau.
Si vous notez une différence entre les deux relevés, c’est que votre installation intérieure fuit. Vérifiez  votre équipement (robinetterie, chasse d’eau…). Attention cette dernière n’est peut être pas visible ou minime. Ne sous estimez pas cette situation, sinon il pourrait vous en coûter cher.</p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone"> Appel d'Urgence</a>
              </div>
            </div>
		<hr>
             <h2>Débouchage-Bordeaux.fr c'est aussi...</h2> 
			<div class="row">
			               <div class="grid_3"><amp-img src="images/canalisation-bouchee-racines.jpg"  width="370" height="217"  alt="Racines bouchant une canalisation"></amp-img>
                <h3>Vous avez dit "Racines"?</h3>
                <p>Les racines vont là ou bon leur semble, et ce n'est pas vos canalisations qui les-en empêcheront. Tèrs régulièrement Débouchage Bordeaux intervient pour le déracinnage des canalisations. Les racines perforent la canalisations et s'ngouffrent à l'intérieur provocant un bouchon dans la canalisation en retenant les matières présentent dans les eaux usées. C'est en procédant à un passage caméra dans la canalisation que l'on peut constater de tel problème de bouchon.</p>
				<a href="contacts.php" class="btn">Prendre Rendez-vous</a>
				</div>
                 <div class="grid_4"><amp-img src="images/drain-sanitation.jpg"  width="370" height="217"  alt="Nettoyage par hydrocurage des drains d'épandage assainissement individuel"></amp-img>
                <h3>Hydrocurage drains d'épandage</h3>
                <p>Il est fréquent que les drains d'un système d'épandage se bouchent. Il est alors important de procéder au débouchage par hydrocurage pour un retour au bon fonctionnement de l'évacuation des eaux usées.</p>
				<a href="contacts.php" class="btn">Prendre Rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'Urgence</a>
              </div>
			<div class="grid_4"><amp-img src="images/debouchage-wc-jet-haute-pression.jpg"   id="debouchage-wc-haute-pression"  width="370" height="217"  alt="Débouchage wc par Haute pression"></amp-img>
                <h3>Débouchage WC Haute Pression</h3>
                <p>Pour déboucher vos wc par la technique de haute pression, nos techniciens utilisent un jet propulsant de l'eau sous haute pression afin de disloquer le bouchon qui colmate la canalisation des toilettes</p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone"> Appel d'Urgence</a>				
              </div>
            </div>
          </div>
        </section>
        <section class="well1 ins4 bg-image-waterleak">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Recherche de fuite nn destructive</h2>
                <p>La recherche non destructive de fuite a pour objectif la localisation rapide sans endommager les canalisations.  <br/>Débouchage-Bordeaux utilise des techniques modernes comme l’inspection télévisée, l’inspection electro acoustique sans pour autant laisser de côté des techniques plus traditionnelles comme les gaz traceurs ou les colorants.</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>Recherche acoustique</li>
                      <li>Recherche par gaz traceur</li>					  
                      <li>Recherche par caméra thermique</li>
                      <li>Recherche par colorant – Fluorescéine</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>Camréa vidéo inspection</li>
                      <li>Hydrocurage</li>
                      <li>Débouchage</li>
                      <li>Nettoyage Bac à Graisse</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="well1">
          <div class="container">
          <div class="container">
            <h2 class="mobile-center" id="tarifs-debouchage">Nos Tarifs</h2>
            <div class="row">
              <div class="grid_4">
                <table class="wow fadeInUp">
                  <tr>
                    <td>Débouchage Manuel</td>
                    <td>à partir de 99€ HT </td>
                  </tr>
                  <tr>
                    <td>Débouchage Haute Pression</td>
                    <td>249€ HT</td>
                  </tr> 
				  <tr>
                    <td>Hydrocurage</td>
                    <td> à partir de 249€ HT</td>
                  </tr>
                  <tr>
                    <td>Inspection Vidéo</td>
                    <td>249€ HT </td>
                  </tr>
                  <tr>
                    <td>Recherche de Fuite</td>
                    <td>249€ HT</td>
                  </tr>
                </table>
              </div>

              <div class="grid_4">
<ul>
<li>Hors taxes:
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> particuliers = +10%,</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> professionnels = +20%</li></li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Notre prestation de "Débouchage manuel" / "débouchage domestique" comprend le débouchage d’un évier, siphon, bonde à l'aide d'une pompe manuelle ou d'un furet.  Le débouchage domestique ne comprends pas les prestations de curage et/ou hydrocurage des canalisations.</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Obligation du client : <br/>
Lors d’une prestation en hydrocurage : Mettre à disposition de notre technicien une arrivée d’eau.</li>
</ul>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html ⚡>