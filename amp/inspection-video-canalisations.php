<!DOCTYPE html>
<html ⚡>
  <head>
    <title>Inspection vidéo canalisations</title>
 <meta charset="utf-8"><link rel="canonical" href="https://www.debouchage-bordeaux.fr/" />
	
	<meta name="description" content="Inspection des canalisations par caméra vidéo pour déterminer la causes des problème d'évacuation des eaux usées">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
		<?php   require('inc/meta.php') ?> 
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/FaviconMister-S.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><amp-img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"  width="370" height="217"  alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html ⚡>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
	  <?php $active = 'video-pipe-inspection'; ?>
          <?php require('inc/menuheader.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins2 mobile-center">
           <div class="container hr well1 ins2">
            <div class="row">
              <div class="grid_6">
			  <h2>Inspection des canalisations par vidéo caméra</h2>
                <div class="video">
<iframe  src="https://www.youtube.com/embed/1z75iOVUxmE" frameborder="0" allowfullscreen></iframe>                </div>
              </div>
              <div class="grid_6">
                <div class="row">
             <div class="grid_3"><amp-img src="images/clogged-pipe.jpg"  width="370" height="217"  alt="Tartre bouchant un tuyau"></amp-img>
                <h3>D'ou vient cette fuite?</h3>
                <p>L'inspection vidéo caméra des canalisation permet de localiser au cm près le problème affectant vos canalisations. Que ce soit un bouchon solide, des racines ou une canalalisation cassée, la caméra vidéo apporte la preuve exacte de votre probleme d'écoulement.</p>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'Urgence</a>
              </div>
			               <div class="grid_3"><amp-img src="images/canalisation-bouchee-racines.jpg"  width="370" height="217"  alt="Racines bouchant une canalisation"></amp-img>
                <h3>Vous avez dit "Racines"?</h3>
                <p>Les racines vont là ou bon leur semble, et ce n'est pas vos canalisations qui les-en empêcheront. Tèrs régulièrement Débouchage Bordeaux intervient pour le déracinnage des canalisations. Les racines perforent la canalisations et s'ngouffrent à l'intérieur provocant un bouchon dans la canalisation en retenant les matières présentent dans les eaux usées. C'est en procédant à un passage caméra dans la canalisation que l'on peut constater de tel problème de bouchon.</p>
				<a href="contacts.php" class="btn">Prendre Rendez-vous</a>
				</div>

                  </div>
                </div>
              </div>
            </div>
		            <div class="container">
            <h2>Débouchage-Bordeaux.fr c'est aussi...</h2> 
			<div class="row">
              <div class="grid_4"><amp-img src="images/water-leak-detection.jpg"  width="370" height="217"  alt="Recherche de fuite"></amp-img>
                <h3>Recherche de Fuite</h3>
                <p>Les fuites d'eaux sont monaie courante dansun logement. Visible ou invisible, votre fuite d'eau s'aggravera avec le temps et c'est votre facture d'eau qui vous le prouvera.<br/>Pour éviter un dégât des eaux, mais aussi une note salée, contactez nous au plus vite.</p>
				<a href="recherche-fuite-eau.php" class="btn">+ d'infos</a>
              </div>
                 <div class="grid_4"><amp-img src="images/drain-sanitation.jpg"  width="370" height="217"  alt="Nettoyage par hydrocurage des drains d'épandage assainissement individuel"></amp-img>
                <h3>Hydrocurage drains d'épandage</h3>
                <p>Il est fréquent que les drains d'un système d'épandage se bouchent. Il est alors important de procéder au débouchage par hydrocurage pour un retour au bon fonctionnement de l'évacuation des eaux usées.</p>
				<a href="contacts.php" class="btn">Prendre Rendez-vous</a>
				<a href="tel:0533080808" class="btpho btn  fa-phone">  Appel d'Urgence</a>
              </div>
			<div class="grid_4"><amp-img src="images/debouchage-wc-jet-haute-pression.jpg"   id="debouchage-wc-haute-pression"  width="370" height="217"  alt="Débouchage wc par Haute pression"></amp-img>
                <h3>Débouchage WC Haute Pression</h3>
                <p>Pour déboucher vos wc par la technique de haute pression, nos techniciens utilisent un jet propulsant de l'eau sous haute pression afin de disloquer le bouchon qui colmate la canalisation des toilettes</p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel:0630061182" class="btpho btn  fa-phone"> Appel d'Urgence</a>				
              </div>
            </div>
          </div>
          </div>

         </section>

        <section class="well1 ins4 bg-image-videocamera">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Inspection des canalisations par vidéo caméra</h2>
                <p>Si vous constatez un problème d'écoulement dans vos canalisations, n'attendez pas plus longtemps pour nous contacter afin d'établir un diagnostic complet de vos canalisations.</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>Inspection vidéo des canalisations</li>
                      <li>Détection vidéo des bouchons</li>					  
                      <li>Détection vidéo des racines</li>
                      <li>Recherche de fuites</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="well1">
          <div class="container">
         <div class="container">
            <h2 class="mobile-center" id="tarifs-debouchage">Nos Tarifs</h2>
            <div class="row">
              <div class="grid_4">
                <table class="wow fadeInUp">
                  <tr>
                    <td>Débouchage Manuel</td>
                    <td>à partir de 99€ HT </td>
                  </tr>
                  <tr>
                    <td>Débouchage Haute Pression</td>
                    <td>249€ HT</td>
                  </tr> 
				  <tr>
                    <td>Hydrocurage</td>
                    <td> à partir de 249€ HT</td>
                  </tr>
                  <tr>
                    <td>Inspection Vidéo</td>
                    <td>249€ HT </td>
                  </tr>
                  <tr>
                    <td>Recherche de Fuite</td>
                    <td>249€ HT</td>
                  </tr>
                </table>
              </div>

              <div class="grid_4">
<ul>
<li>Hors taxes:
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> particuliers = +10%,</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> professionnels = +20%</li></li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Notre prestation de "Débouchage manuel" / "débouchage domestique" comprend le débouchage d’un évier, siphon, bonde à l'aide d'une pompe manuelle ou d'un furet.  Le débouchage domestique ne comprends pas les prestations de curage et/ou hydrocurage des canalisations.</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Obligation du client : <br/>
Lors d’une prestation en hydrocurage : Mettre à disposition de notre technicien une arrivée d’eau.</li>
</ul>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>

  </body>
</html ⚡>