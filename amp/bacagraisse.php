<!DOCTYPE html>
<html ⚡>
  <head>
    <title>Vidange bac à graisse</title>
    <meta charset="utf-8">
	
	<meta name="description" content="Mister Service provide grease trap cleaning & pumping service.  Get in touch, call to 305 504 2727. Available 24/7 on Miami">
	<?php   require('inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/FaviconMister-S.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><amp-img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html ⚡>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
          <?php require('inc/menuheader.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins2 mobile-center">
          <div class="container">
            <h2>The best Grease Trap services</h2>
            <div class="row off2">
			<div class="grid_4"><amp-img src="images/grease-trap.jpg" alt="" width="370" height="217"></amp-img>
                <h3>Grease Trap ?</h3>
                <p>The grease trap acts as a filter trapping grease inside and allowing water to flow out to the sewer. The purpose of a grease trap is to prevent the fats, oils, and grease from clogging sewer infrastructure.</p><a href="#" class="btn">I need One</a>
              </div>
			<div class="grid_4"><amp-img src="images/pumping-grease-trap.jpg" width="370" height="217"  id="pumping-grease-trap" alt="Mister Service Miami Pump grease trap"></amp-img>
                <h3>Grease Trap Pumping & Cleaning</h3>
                <p>Why pump and clean a grease trap ? Regular maintenance of grease trap affects its efficiency (60 % to 70 %). If the grease trap is not pump and clean , the surface layer of grease will thicken and , after a few weeks, she will begin to ferment and cause bad smell.</p><a href="contacts.php#myform" class="btn">I need it</a>   <a href="callto:001 305 504 2727" class="btpho btn  fa-phone">  EMERGENCY</a>
              </div>
                 <div class="grid_4"><amp-img src="images/grease-trap-unclogging.jpg" width="370" height="217" alt=""></amp-img>
                <h3>Unclogging Grease Trap</h3>
                <p>If you do not clean your grease trap, it can overflow and clog. The grease solidify and clog the drainage system. Wastewater will flow over and cause stinky odors. <br/> To remedy this problem, we unclogging drains your grease trap</p><a href="contacts.php#myform" class="btn">I need it</a>   <a href="callto:001 305 504 2727" class="btpho btn  fa-phone">  EMERGENCY</a>
              </div>
            </div>
		<hr>
            <h2>The best other sanitation services</h2> 
			<div class="row">
              <div class="grid_4"><amp-img src="images/water-leak-detection.jpg" width="370" height="217" alt="water leak detection with a thermal camera "></amp-img>
                <h3>Water Leak Detection</h3>
                <p>Water leaks are always a problem. They are usually just a nuisance, a minor annoyance, but in certain situations they can start to devalue your home</p>
				<a href="water-leak-detection.php" class="btn">Read more</a>
              </div>
              <div class="grid_4"><amp-img src="images/camera-video-inspection.jpg"  width="370" height="217" alt="watch on your pipe with camer video inspection"></amp-img>
                <h3>Drain Camera Video Inspection</h3>
                <p>Water no longer flows in pipes correctly ? Worse, it seems to stagnate ? To know the cause of your piping problems, we spend a video camera in the pipes. We will find a clogging grease, roots or can be an a-ligator, who knows ?  At the end we will send you a video report.</p><a href="video-pipe-inspection.php" class="btn">Read more</a>
              </div>
              <div class="grid_4"><amp-img src="images/hydrojetting.jpg" width="370" height="217" alt="Hydrojetting uncloging pipes"></amp-img>
                <h3>Hydrojetting</h3>
                <p>Hуdrо Jеt drаіn сlеаnіng іѕ an extremely аffесtіvе service that is uѕеd when уоu hаvе a sewer drain thаt is сlоggеd. Ovеr time уоur ѕеwеr lіnе and drains, lіkе thе kіtсhеn ѕіnk drain, buіld up dеbrіѕ thаt ѕеttlеѕ іn thе drаіn сrеаtіng ѕludgе like ѕubѕtаnсе thаt ѕtауѕ іn the drain fоr good.</p><a href="hydro-jetting.php" class="btn">Read more</a>
              </div>
            </div>
          </div>
        </section>
        <section class="well1 ins4 bg-image">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Grease Trap Service</h2>
                <p>When you call us  you get the most friendly, knowledgeable and experience staff in the grease trap industry in the south florida. Get the best grease trap service in Miami. With our vaccum truck we respond to any emergency promptly. <br/>Mister Service = FAIR PRICE BUT SUPERIOR SERVICE</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>Grease Trap Pumping</li>
                      <li>Grease Trap Cleaning</li>					  
                      <li>Grease Trap Installation</li>
                      <li>Grease Trap Maintenance</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>Pressure Cleaning</li>
                      <li>Water Jetting</li>
                      <li>Unclogging</li>
                      <li>Floor Drain Cleaning</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="well1">
          <div class="container">
            <h2 class="mobile-center">Price list</h2>
            <div class="row">
              <div class="grid_4">
                <table class="wow fadeInUp">
                  <tr>
                    <td>Grease Trap Pumping</td>
                    <td>$ 350.00 < 1000 gal </td>
                  </tr>
                  <tr>
                    <td>Pressure Cleaning</td>
                    <td>$ 200.00</td>
                  </tr>
                  <tr>
                    <td>Grease Trap Installation</td>
                    <td>on demand</td>
                  </tr>

                </table>
              </div>
              <div class="grid_4">
                <table data-wow-delay="0.2s" class="wow fadeInUp">
                  <tr>
                    <td>Grease Trap Maintenance</td>
                    <td>on demand</td>
                  </tr>
                  <tr>
                    <td>Septic Tank Pumping</td>
                    <td>$ 325.00 <1000 gal</td>
                  </tr>
                  <tr>
                    <td>Water Jetting</td>
                    <td>$ 250.00</td>
                  </tr>

                </table>
              </div>
              <div class="grid_4">
                <table data-wow-delay="0.4s" class="wow fadeInUp">
                  <tr>
                    <td>Unclogging</td>
                    <td>$270.00</td>
                  </tr>
                  <tr>
                    <td>Water Leak Detection</td>
                    <td>$ 300.00</td>
                  </tr>
                  <tr>
                    <td>Caméra Video Inspection</td>
                    <td>$ 250.00</td>
                  </tr>
 
                </table>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="js/script.js"></script>

  </body>
</html ⚡>