<?php 
echo'

		

	<script type="application/ld+json"> 
{
  "@context": "http://www.schema.org",
  "@type": "Plumber",
  "name": "Débouchage-Bordeaux.fr",
  "url": "https://www.debouchage-bordeaux.fr",
  "logo": "https://www.debouchage-bordeaux.fr/images/logo-debouchagebordeaux.png",
  "image": "https://www.debouchage-bordeaux.fr/images/debouchage-wc-pompe-manuelle.jpg",
  "description": "AG Débouchage Bordeaux, marque d\'AG Assainissement, intervient pour le débouchage wc, le débouchage canalisations, l\'hydrocurage canalisations, la recherche de fuite, l\'inspection vidéo des canalisations, la détection de fuite d\'eau, la réparation de cumulus, l\'entretien de sanibroyeur, le remplacement de sanibroyeur, la soudure sur tuyaux cuivre cuivre, les réparation de plomberie. Ag Débouchage Bordeaux intervient à Bordeaux (33 Gironde), sur Bordeaux Métropole, et partout en Gironde sur simple appel au 05 33 08 08 08",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "31 rue Aristide Bergès",
    "addressLocality": "FLOIRAC",
    "addressRegion": "Gironde",
    "postalCode": "33270",
    "addressCountry": "France"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": "44.8223132",
    "longitude": "-0.5267011"
  },
  "hasMap": "https://www.google.fr/maps/place/AG+Assainissement+Floirac/@44.8223132,-0.5267011,15z/data=!4m12!1m6!3m5!1s0x0:0xeffb37ddf9a840b3!2sAG+Assainissement+Floirac!8m2!3d44.8223132!4d-0.5267011!3m4!1s0x0:0xeffb37ddf9a840b3!8m2!3d44.8223132!4d-0.5267011",
  "openingHours": "Mo, Tu, We, Th, Fr, Sa, Su 07:00-23:00",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+33533080808",
    "contactType": "customer service",
	"areaServed":"Gironde"
  }
}
 </script>
					
';
?>					


