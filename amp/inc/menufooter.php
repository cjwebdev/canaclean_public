<?php 
echo'

        <section class="well3">
          <div class="container">
            <ul class="row contact-list">
              <li class="grid_4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-map-marker"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <address>31 rue Aristide Bergès<br/>33270 FLOIRAC</address>
                  </div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-envelope"></div>
					</div>
                  <div class="box_cnt__no-flow"><a href="mailto:contact@ag-assainissement.fr">contact@ag-assainissement.fr</a></div>
                </div>
              </li>
              <li class="grid_4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-phone"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="tel:0630061182">06 30 06 11 82</a></div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-fax"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="tel:0533080808">05 33 08 08 08</a></div>
                </div>
              </li>
              <li class="grid_4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-facebook"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="https://www.facebook.com/AG.Assainissement/">Suivez nous sur Facebook AG Assainissement</a></div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-send"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="contacts.php#myform">Restons en contact</a></div>
                </div>
              </li>
            </ul>
          </div>
        </section>
        <section>
          <div class="container">
            <div class="copyright">Débouchage Bordeaux © <span id="copyright-year">2017</span> Tous droits réservés &nbsp;&nbsp;<br/><b/>
			Débouchage-bordeaux.fr est une marque d\'<a href="https://www.ag-assainissement.fr">AG Assainissement</a></br>|  <a href="./">Accueil</a>  |  <a href="wc.php">Débouchage WC</a>  |  <a href="canalisations.php">Débouchage canalisations</a>  |  <a href="hydrocurage-canalisations.php">Hydrocurage</a>  |  <a href="recherche-fuite-eau.php">Recherche de fuite</a>  |  <a href="inspection-video-canalisations.php">Inspection vidéo caméra des canalisations</a>  |  <a href="mentions-legales.php">Mentions légales</a>  |  <a href="contacts.php">Contacts</a></div>
						
		<div class="grid_5 ">
			
			<p>Nos permanences en Gironde<br/>
			<a href="merignac.php#myform" alt="Contactez la permanence de Mérignac" >Mérignac</a>  |
			<a href="pessac.php#myform" alt="Contactez la permanence de Pessac" >Pessac</a>  |  
			<a href="pauillac.php#myform" alt="Contactez la permanence de Pauillac" >Pauillac</a>  |  
			<a href="libourne.php#myform" alt="Contactez la permanence de Libourne" >Libourne</a>  |  
			  <a href="langon.php#myform" alt="Contactez la permanence de Langon" >Langon</a>  |  
				  <a href="arcachon.php#myform" alt="Contactez la permanence d\'Arcachon" >Arcachon</a> </p> 
            </div>
			<div class="grid_5 "><a href="https://www.ag-plombier-bordeaux.fr">Nos services de plomberie à Bordeaux :<br/> AG-plombier-bordeaux.fr</a></div>

          
		 
        </section>
					
;'
?>					