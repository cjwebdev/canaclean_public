<?php 
echo'

<style amp-custom>

@import url(//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css);


body {
  background: #f5f5f5;
  color: #56585a;
  font: 400 14px/28px "Roboto", sans-serif;
  -webkit-text-size-adjust: none;
  padding:0 3px;
}

.brand {
	text-align:center;
}


  amp-img {
	  max-width: 100%;
  height: auto;
  }
  
[class*="fa-"]:before {
  font-weight: 350;
  font-family: "FontAwesome";
}  

.brandtoo {
	float:right;
	display:block;
	color:#FFF;
	
}
ul.brandtoo li.brandtoo {
	display:inline;
	float:left;
}
.brandtoo>li {
	padding:5px 10px;
	display:inline-block;
	margin-right:2px;
	background-color:#69ACD7;
	height:70px;
	width: auto;
    text-align: center;
	line-height: initial;
	font-weight:bold;
}
.brandtoo>li>a>i {
	display: block;
    font-size: 2.5em;
    width: 100%;
}
.brandtoo>li>a {
	padding-top:10px;
	color:#FFF;
	text-decoration:none;
}
.brandtoo>li>a:hover {
	color:#434547;
}
.brandtoo>li.db-big {
	margin-right: 0;
	color:#69ACD7;
    width: 231px;
	background-color:#004D73 ;
	text-align: left;
	margin-top:2px;
}

.brandtoo>li.db-big>a:hover {
	color:#FFF;
}
.brandtoo>li.db-big>span>span.txt1 {
    color: #3e3433;
    display: block;
    font-weight: 500;
    line-height: 1.3em;
}
.brandtoo>li.db-big>span>span.txt2 {
        margin-bottom: 6px;
}
.brandtoo>li.db-big>span>span.txt3 {
        font-size: 19px;
    font-weight: bold;
}
.brandtoo>li.db-big>a>i {
    float: left;
	margin:15px 10px;
    height: 70px;
    width: auto;;
} 


.btn,
.btn2 {
  display: inline-block;
  width: 150px;
  padding: 16px;
  font-size: 16px;
  line-height: 18px;
  font-weight: 500;
  text-align: center;
  text-transform: uppercase;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}
.btn:hover,
.btn2:hover {
  background: #4ab5e2;
}
.btn:active,
.btn2:active {
  background: #22a1d7;
}

.btn {
  color: #fff;
  background: #62bfe6;
  	text-decoration:none;
}

.btn2 {
  color: #62bfe6;
  background: #eaeaea;
}
.btn2:hover, .btn2:active {
  color: #fff;
} 


.box:before, .box:after {
  display: table;
  content: "";
  line-height: 0;
}
.box:after {
  clear: both;
}
.box_aside {
  float: left;
}
.box_cnt__no-flow {
  overflow: hidden;
}



footer {
  color: #FFF;
  background-color: #004D73 ;
}

footer a {
	color:#FFF !important;
	text-decoration:none;
}

footer .copyright {
  padding-top: 21px;
  padding-bottom: 55px;
}
footer .copyright a:hover {
  text-decoration: underline;
}
@media (max-width: 767px) {
  footer .copyright {
    text-align: center;
  }
}

footer ul li {
	list-style-type:none;
}

.icon2 {
  position: relative;
  width: 70px;
  height: 70px;
  line-height: 70px;
  font-size: 34px;
  text-align: center;
  color: #62bfe6;
  background: #3D3D3D;
}
.icon2:before {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}


.contact-list {
  color: #fff;
  font-size: 12px;
}
.contact-list .box {
  display: table;
}
.contact-list .box_cnt__no-flow {
  vertical-align: middle;
  display: table-cell;
  padding-left: 30px;
}
.contact-list .box + .box {
  margin-top: 30px;
}
.contact-list a[href^="tel:"],
.contact-list a[href^="tel:"] {
  font-size: 16px;
}
@media (min-width: 768px) and (max-width: 979px) {
	.contact-list a[href^="tel:"],
	.contact-list a[href^="tel:"]  {
    font-size: 18px;
  }
}
.contact-list a:hover {
  color: #62bfe6;
}

.contact-list2 {
  padding-top: 23px;
  padding-bottom: 2px;
}
.contact-list2 h3 {
  color: #42acda;
}
.contact-list2 a[href^="tel:"],
.contact-list2 a[href^="tel:"] {
  font-size: 24px;
  line-height: 28px;
}
.contact-list2 dd, .contact-list2 dt {
  display: inline-block;
  font-size: 16px;
}
.contact-list2 a + dl {
  margin-top: 9px;
}
  
</style>
					
';
?>					


