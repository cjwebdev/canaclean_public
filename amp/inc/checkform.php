			<?php	
	
$s = "";
function verifieEmail($mail) 
{
	if (preg_match('/^[a-zA-Z0-9_]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$]/i',$mail)) return false;
	list ($nom,$domaine) = explode ('@',$mail);
	if (getmxrr($domaine,$mxhosts)) return true;
	else return false;
} 
if (!empty($_POST['nom']) && !empty($_POST['telephone']) && !empty($_POST['email']) && !empty($_POST['message']) )
{
    $destinataire = "contact@ag-assainissement.fr";
    $sujet = "Débouchage-bordeaux.fr : Demande de RDV";
	$message = "Nom : ".$_POST['nom']."\r\n";
    $message .= "Telephone : ".$_POST['telephone']."\r\n";
    $message .= "Email : ".$_POST['email']."\r\n";
    $message .= "Message : ".$_POST['message']."\r\n";
    $from = $_POST['email'];
    if (verifieEmail($from))
    {
        $entete = 'From: '.$from;
        if (mail($destinataire,$sujet,$message,$entete))
        {
            $s = "Votre demande a bien été envoyée. Nous la traiterons dans les plus brefs délai et reviendrons très vite vers vous.";
        }
        else
        {
            $s = "Demande non envoyée, vérifiez vos informations - NOM, Tel, email, message.";
        }
    }
    else
    {
        $s = "Demande non envoyée, merci de recommencer.";
    }
}
else
{
    $s = "Vous n'avez pas rempli TOUS les champs obligatoire, merci de complèter les champs vides.";
}
if ($s) echo $s;

?>