<?php 
echo'

        <section class="well3">
          <div class="container">
            <ul class="row contact-list">
              <li class="grid_4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-map-marker"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <address>rue machin<br/>Bègles</address>
                  </div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-envelope"></div>
					</div>
                  <div class="box_cnt__no-flow"><a href="mailto:contact@canaclean.fr">contact@canaclean.fr</a></div>
                </div>
              </li>
              <li class="grid_4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-phone"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="tel: 0609151330"> 06 09 15 13 30</a></div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-phone"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="tel:0533080808">05 33 08 08 08</a></div>
                </div>
              </li>
              <li class="grid_4">
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-facebook"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="#">Suivez nous sur Facebook</a></div>
                </div>
                <div class="box">
                  <div class="box_aside">
                    <div class="icon2 fa-send"></div>
                  </div>
                  <div class="box_cnt__no-flow"><a href="contacts.php#myform">Restons en contact</a></div>
                </div>
              </li>
            </ul>
          </div>
        </section>
        <section>
          <div class="container">
            <div class="copyright">Canaclean © <span id="copyright-year">2017</span> Tous droits réservés &nbsp;&nbsp;<br/><b/>
			Canaclean, entreprise de débouchage wc & canalisation à Bordeaux. Canaclean débouche les wc et les canalisations en urgence (7/7) sur toute la Gironde. Canaclean répare les canalisations, recherche les fuites, vidange la fosse septique, entretien le bac à graisse des particuliers & professionnels, réalise l\'inspection vidéo du réseau d\'eaux usées et pluviales. </br>|  <a href="">Accueil</a>  |  <a href="deboucher/deboucher-mes-wc-bouches.php">Débouchage WC</a>  |  <a href="deboucher/deboucher-mes-canalisations-bouchees.php">Débouchage canalisations</a>  |  <a href="deboucher/deboucher-hydrocurage.php">Hydrocurage</a>  |  <a href="rechercher/recherche-fuite-eau.php">Recherche de fuite</a>  |  <a href="inspecter/inspection-canalisation-video-camera.php">Inspection vidéo caméra des canalisations</a>  |  <a href="mentions-legales.php">Mentions légales</a>  |  <a href="contacts.php">Contacts</a></div>
        </section>
					
;'
?>