<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Canaclean.fr</title>
  <meta charset="utf-8">
  <meta name="description" content="déboucher mes wc & déboucher une canalisation du lundi au dimanche sur rendez-vous rapide ou en urgence.">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/grid.css">
  <link rel="stylesheet" href="css/camera.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/owl-carousel.css">
  <link rel="stylesheet" href="css/google-map.css">
  <link rel="stylesheet" href="css/step.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="js/jquery.js"></script>
  <script src="js/jquery-migrate-1.2.1.js"></script>
  <!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="js/html5shiv.js"></script><![endif]-->
  <script src="js/device.min.js"></script>
</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=905277863011455";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
  <div class="page">
    <!--
      ========================================================
      							HEADER
      ========================================================
     -->
    <header>
      <?php require('inc/menuheader.php'); ?>
    </header>
    <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
    <main>
      <section class="camera_container eligible">
        <div class="container">
          <div class="row">
            <center>
              <h1>Entrez votre code postal pour connaître votre éligibilité</h1>
                <?php if (isset($_GET['eligible'])){?>
                  <p style="color:red">Votre commune n'est pas eligible</p>
                <?php } ?>
              <form id="step1Form" class="" action="backend/eligibility.php" method="post">
                <input id="user_cp" type="text" name="cp" value="" placeholder="Entre ton code postal">
              </form>
            </center>
          </div>
        </div>
		</section>
	  <!-- =======================================================
map-marker

	  ============================================================
	  -->
		<section class="well1 ins2 mobile-center">
            <div class="container">
              <h2>Commandez et payez en ligne votre prestation</h2>
              <div class="row fadeInRight animated">
				  <div class="grid_4">
                  <h3><i class="fa fa-map-marker" aria-hidden="true"></i> Localisez</h3>                 
                    <p><strong>1.</strong> En localisant votre commune où l'intervention aura lieu nous pourrons calculer au plus juste notre tarif.</p>                   
                </div>
                <div class="grid_4">
                  <h3><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Commandez</h3>                 
                  <p><strong>2.</strong> Sélectionnez la prestation Canaclean dont vous avez besoin parmi nos services: vidange de fosse septique, vidange de bac à graisse, débouchage manuel, hydrocurage canalisation, recherche de fuite, inspection vidéo des canalisations, traçage des réseaux EP-EU. </p>                
                </div>
                <div class="grid_4">
                  <h3><i class="fa fa-eur" aria-hidden="true"></i> Reglez en ligne</h3>
                     <p><strong>3.</strong> Renseignez vous informations (civilité, coordonnées) puis régler directement en ligne par carte bleue sur notre solution de transaction sécurisée. Une fois le paiement validé, nous revenons vers vous dans la journée pour fixer le rendez vous d'intervention. (entre 24 et 72h)
                    </p>               
                </div>
              </div>
            </div>
          </section>  
	  <!-- =======================================================

	  ============================================================
	  -->
      
      <section class="map">
        <div id="map" class="map_model">
          <ul class="map_locations">
            <li data-x="-80.14400009999997" data-y="25.9683634">
              <div class=" infonmap">
                <div class=" infmap">
                  <div class="preffix_5 grid_5">
                    <h2>Vidanger<br /> ma fosse septique</h2>
                    <p>La vidange de fosse septique ou de tout autre système d'assainissment non collectif </p>
                    <a  class="btnC_C " href="contacts.php#myform" >J'en profite</a>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </section>
          <section class="well1 ins2 mobile-center">
            <div class="container">
              <h2>Informations & Conditions particulières</h2>
              <div class="row off2 HomeBloc">
                <div class="grid_4 HomeBloc">
                  <h3>Paiement en ligne sécurisé</h3>
                  <div>
                    <img src="images/payzen_paiement-secure_ill_info.jpg" alt="Paiement en ligne sécurisé par Payzen de Lyra">
                    <p><strong>Commandez et payer en ligne.</strong> Vous pouvez désormais commmander votre prestation sur ce site et réglez le montant directement grâce à notre paiement en ligne sécurisé par la solution Payzen de Lyra.</p>
                    <a href="https://payzen.eu/" title="Payzen par Lyra, solution e-commerce pour le paiement en ligne sécurisé">En savoir + sur Payzen</a></div>
                </div>
                <div class="grid_4">
                  <h3>Débouchage WC</h3>
                  <img src="images/debouchage-wc-pompe-manuelle.jpg" alt="débouchage wc avec une pompe manuelle">
                  <p>Avoir ses wc bouchés n'est jamais une bonne nouvelle et peut très vite devenir une urgence. Ne perdez pas
                    votre temps en produits de débouchage qui sont ultra nocifs: contactez-nous pour une intervention débouchage
                    efficace dans la journée. </p>
                  <a href="wc.php" title="Débouchage-bordeaux.fr : Conseils et intervention wc bouchés">+ d'infos wc bouchés</a>
                </div>
                <div class="grid_4">
                  <h3>Débouchage canalisations</h3>
                  <img src="images/canalisation-bouchee.jpg" alt="Canalisation bouchée par la graisse des eaux usées">
                  <p>Une canalisation bouchée peut vite devenir vous causer de sérieux problèmes d'évacuation des eaux usées,
                    et des dégâts certains. Les causes du bouchon dans la canalisations peuvent être multiples (graisses
                    accumulés, cheveux, lingettes...), mais pas d'inquiétude, le débouchage aura bien lieu en nous contactant.
                    </p>
                  <a href="canalisations.php">+ d'info sur le débouchage</a>
                </div>

              </div>

              <div class="row HomeBloc">
                <div class="grid_4 HomeBloc">
                  <h3>Recherche de Fuite</h3>
                  <img src="images/water-leak-detection.jpg" alt="Recherche de fuite">
                  <p>Les fuites d'eaux sont monaie courante dansun logement. Visible ou invisible, votre fuite d'eau s'aggravera
                    avec le temps et c'est votre facture d'eau qui vous le prouvera.<br/>Pour éviter un dégât des eaux, mais
                    aussi une note salée, contactez nous au plus vite.</p>
                  <a href="recherche-fuite-eau.php">+ d'infos</a>
                </div>
                <div class="grid_4">
                  <h3>Inspection vidéo canalisations</h3>
                  <img src="images/camera-video-inspection.jpg" alt="Inspection des canalisations par caméra vidéo">
                  <p>Pour connaitre la cause de vos problèmes d'écoulement des eaux (lent ou inexistant), l'inspection des canalisations
                    par vidéo caméra en est la solution. Racines invasives, effondrement de paroi, canalisations cassée,
                    bouchon, colmatage, le passage caméra dans les canalisations apporte la preuve en vidéo. </p>
                  <a href="inspection-video-canalisations.php">+ d'infos</a>
                </div>
                <div class="grid_4">
                  <h3>Hydrocurage Canalisations</h3>
                  <img src="images/hydrocurage-avant-apres.jpg" alt="Hydrocurage de canalisations bouchées">
                  <p>Pour conserver des canalisations avec un diamètre d'écoulement originel, rien de tel qu'un hydrocurage.
                    La tête hydrocureuse avance en disloquant les matières et nettoie les parois de vos canalisations. N'attendez
                    pas d'avoir un problème de canalisation bouchée pour nous contacter.</p>
                  <a href="hydrocurage-canalisations.php">+ d'infos</a>
                </div>
              </div>
            </div>
            <hr>
          </section>
          <section class=" ins1">
            <div class="hbl">
              <div class="container">
                <div class="row ">
                  <div class="grid_3">
                    <div><img src="images/contact-canaclean.jpg" alt=" - Contact">
                    </div>

                  </div>
                  <div class="grid_4">
                    <div>
                      <h3><i class="fa fa-exclamation-triangle"></i> URGENCE DÉBOUCHAGE</h3>

                      <p class="text-justify">Pour toute urgence de canalisations intérieures ou extérieures bouchées, Canaclean intervient chez
                        vous 24h/24 et 7j/7 sur Bordeaux et dans toute la Gironde.Tarifs clairement communiqués en fonction
                        des problèmes décrits lors de votre appel.</p><a href="tel:+33609151330" class="btnC_C "><i class="fa fa-phone"></i> 06 09 15 13 30</a>
                    </div>
                  </div>
                  <div class="grid_5 ">
                    <h4>PARTICULIERS, PROFESSIONNELS ou SYNDICATS DE COPROPRIÉTÉ</h4>
                    <p class="text-justify">Canaclean, entreprise spécialisée dans l'assainissement intervient chez vous pour résoudre vos problèmes
                      de canalisations bouchées ou tout autre problème d’assainissement. Canaclean c’est une garantie de
                      satisfaction et de services au meilleur prix. Vous souhaitez un débouchage de canalisation ? Une inspection
                      vidéo ?</p>
                    <a href="/contact/" class="btnC_C">Contactez nous</a>
                  </div>
                </div>
              </div>
          </section>

    </main>
    <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
    <footer>
      <?php require('inc/menufooter.php') ?>
    </footer>
    </div>

    <script src="js/script.js"></script>
     <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="js/step1.js"></script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDV5UklTpQO4apEm8S5_LIdjNHeeEVJIkQ&callback=initMap"></script>
    <script>
 $( function() {
     var ville = []
     $.get( "backend/getAreas.php", function( data ) {

         for(var i = 0;i < data.length;i++){
             ville.push(""+ data[i].name +" "+ data[i].cp +"")
         }

        var availableTags = ville;
        $( "#user_cp" ).autocomplete({
        source: availableTags,
        select: function (e, ui) {
          var str = new String(ui.item.value)
          var rev = str.split('').reverse().join('');
          var cp = rev.split(' ')[0];
          cp = cp.split('').reverse().join('');
          
          var tab = str.split(' ');
          
          tab.pop();
          var str = "";
          for (var i = 0; i < tab.length; i++){
            str += tab[i];
            if (i < tab.length -1)
            str += ' '
          }
      
          window.location = "./backend/eligibility.php?name=" + str + "&cp=" + cp;
        }

        });
        });
        });

    </script>
    <script>
      (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-70918441-6', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>
