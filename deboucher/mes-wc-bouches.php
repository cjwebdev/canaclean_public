
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>mes WC bouchés</title>
    <meta charset="utf-8">
	
	<meta name="description" content="Débouchage WC par pompe manuelle ou hydrocurage selon l'importance et la localisation du bouchon dans la canalisation wc.">
	<?php   require('inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
          <?php require('inc/menuheader.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins2 mobile-center">
          <div class="container">
            <h2>Mes WC sont bouchés, j'ai besoin ...</h2>
            <div class="row off2">
			<div class="grid_4"><img src="images/wc-debouchage-pompe-manuelle.jpg" alt="Wc bouchés, débouchage avec une pompe manuelle">
                <h3>Débouchage WC Manuel</h3>
                <p>Il est possible de déboucher un wc avec une pompe manuelle spéciale ou un furet si tenté que le bouchon ne soit ni trop loin ni trop conséquent. Ces techniques de débouchage éprouvées conviennent dans 50% des cas. Pour le vérifier, contactez-nous</p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel: 0609151330" class="btpho btn  fa-phone"> Appel d'Urgence</a>
              </div>
			<div class="grid_4"><img src="images/debouchage-wc-jet-haute-pression.jpg"   id="debouchage-wc-haute-pression" alt="Débouchage wc par Haute pression">
                <h3>Débouchage WC Haute Pression</h3>
                <p>Pour déboucher vos wc par la technique de haute pression, nos techniciens utilisent un jet propulsant de l'eau sous haute pression afin de disloquer le bouchon qui colmate la canalisation des toilettes</p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel: 0609151330" class="btpho btn  fa-phone"> Appel d'Urgence</a>				
              </div>
                 <div class="grid_4"><img src="images/debouchage-100pour100-ecologique.png" alt="Mister Service Miami provide unclogging service at your home">
                <h3>Débouchage 100% Écologique</h3>
                <p>Le débouchage 100% écologique est réalisé SANS produits nocifs pour l'environnement et votre santé. Ce débouchage n'altère pas les canalisations car il est totalement dépourvu de produits corrosifs. </p>
				<a href="contacts.php#myform" class="btn" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
				<a href="tel: 0609151330" class="btpho btn  fa-phone"> Appel d'Urgence</a>				
              </div>
            </div>
		<hr>
            <h2>Débouchage-Bordeaux.fr c'est aussi...</h2> 
			<div class="row">
              <div class="grid_4"><img src="images/water-leak-detection.jpg" alt="Recherche de fuite">
                <h3>Recherche de Fuite</h3>
                <p>Les fuites d'eaux sont monaie courante dansun logement. Visible ou invisible, votre fuite d'eau s'aggravera avec le temps et c'est votre facture d'eau qui vous le prouvera.<br/>Pour éviter un dégât des eaux, mais aussi une note salée, contactez nous au plus vite.</p>
				<a href="recherche-fuite-eau.php" class="btn">+ d'infos</a>
              </div>
              <div class="grid_4"><img src="images/camera-video-inspection.jpg" alt="Inspection des canalisations par caméra vidéo">
                <h3>Inspection vidéo des canalisations</h3>
                <p>Pour connaitre la cause de vos problèmes d'écoulement des eaux (lent ou inexistant), l'inspection des canalisations par vidéo caméra en est la solution. Racines invasives, effondrement de paroi, canalisations cassée, bouchon, colmatage, le passage caméra dans les canalisations apporte la preuve en vidéo. </p>
				<a href="inspection-video-canalisations.php" class="btn">+ d'infos</a>
              </div>
              <div class="grid_4"><img src="images/hydrocurage-avant-apres.jpg" alt="Hydrocurage de canalisations bouchées">
                <h3>Hydrocurage Canalisations</h3>
                <p>Pour conserver des canalisations avec un diamètre d'écoulement originel, rien de tel qu'un hydrocurage. La tête hydrocureuse avance en disloquant les matières et nettoie les parois de vos canalisations. N'attendez pas d'avoir un problème de canalisation bouchée pour nous contacter.</p>
				<a href="hydrocurage-canalisations.php" class="btn">+ d'infos</a>
              </div>
            </div>
          </div>
        </section>
        <section class="well1 ins4 bg-image-wc">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Débouchage Service</h2>
                <p>Nous nous déplaçons en urgence ou sur rendez-vous pour déboucher vos toilettes à Bordeaux et dans toute la Gironde. Fort de notre expérience et de notre matériel, nous serons à même de déboucher vos wc sans produits nocifs ou corrosifs pour protéger votre santé mais aussi l'environement.</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>WC bouché</li>
                      <li>Toilettes bouchées</li>					  
                      <li>Sanibroyeur bouché</li>
                      <li>Sanibroyeur bloqué</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>Débouchage Haute pression</li>
                      <li>Débouchage 100% écologique</li>
                      <li>Débouchage Pompe Manuelle</li>
                      <li>Débouchage Furet</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="well1">
          <div class="container">
            <h2 class="mobile-center">Tarif débouchage wc</h2>
             <div class="row">
              <div class="grid_4">
                <table class="wow fadeInUp">
                  <tr>
                    <td>Débouchage Manuel</td>
                    <td>à partir de 99€ HT </td>
                  </tr>
                  <tr>
                    <td>Débouchage Haute Pression</td>
                    <td>249€ HT</td>
                  </tr> 
				  <tr>
                    <td>Hydrocurage</td>
                    <td> à partir de 249€ HT</td>
                  </tr>
                  <tr>
                    <td>Inspection Vidéo</td>
                    <td>249€ HT </td>
                  </tr>
                  <tr>
                    <td>Recherche de Fuite</td>
                    <td>249€ HT</td>
                  </tr>
                </table>
              </div>

              <div class="grid_4">
<ul>
<li>Hors taxes:
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> particuliers = +10%,</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> professionnels = +20%</li></li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Notre prestation de "Débouchage manuel" / "débouchage domestique" comprend le débouchage d’un évier, siphon, bonde à l'aide d'une pompe manuelle ou d'un furet.  Le débouchage domestique ne comprends pas les prestations de curage et/ou hydrocurage des canalisations.</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Obligation du client : <br/>
Lors d’une prestation en hydrocurage : Mettre à disposition de notre technicien une arrivée d’eau.</li>
</ul>
              </div>
            </div>
          </div>
          </div>
        </section>
      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>