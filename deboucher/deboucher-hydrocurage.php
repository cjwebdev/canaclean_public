
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Déboucher par hydrocurage</title>
    <meta charset="utf-8">
	<meta name="description" content="Débouchage WC par hydrocurage haute pression, 100% écologique, sans ajout de produits nocifs pour l'environnement ou votre santé.">
	<?php   require('../inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/grid.css">
    <link rel="stylesheet" href="../css/style.css">
	    <link rel="stylesheet" href="../css/tarifs.css">
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="../images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="../js/html5shiv.js"></script><![endif]-->
    <script src="../js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      -->
      <header>
          <?php require('../inc/menuheader-inner.php')?> 
      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins4 bg-image-hydrocurage">
			<div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Déboucher par hydrocurage</h2>
                <p>Nous nous déplaçons en urgence ou sur rendez-vous pour déboucher vos toilettes à Bordeaux et dans toute la Gironde. Fort de notre expérience et de notre matériel, nous serons à même de déboucher vos wc sans produits nocifs ou corrosifs pour protéger votre santé mais aussi l'environement.</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>Débouchage haute pression</li>
                      <li>Détartrage canalisation</li>					  
                      <li>Curage</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>100% écologique</li>
                      <li>Débouchage 100% écologique</li>
                      <li>Débouchage Pompe Manuelle</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
	  <section class="well1 ins2 mobile-center">
          <div class="container">
            <h2>Curage, hydrocurage des canalisations</h2>
            <div class="row off2 HomeBloc">
			<div class="grid_4 HomeBloc">
				<h3>Débouchage wc /canalisations</h3>
				<img src="../images/wc-debouchage-pompe-manuelle.jpg" alt="Wc bouchés, débouchage avec une pompe manuelle">
                <p>Il est possible de déboucher un wc avec une pompe manuelle spéciale ou un furet si tenté que le bouchon ne soit ni trop loin ni trop conséquent. Ces techniques de débouchage éprouvées conviennent dans 50% des cas. Pour le vérifier, contactez-nous</p>
				<a href="<?php echo $url; ?>step1.php" >Découvrir</a>
				<a href="tel: 0609151330" class="   fa-phone"> Appel d'Urgence</a>
              </div>
			<div class="grid_4">
				<h3>Détartrage canalisations</h3>
				<img src="../images/debouchage-wc-jet-haute-pression.jpg" id="debouchage-wc-haute-pression" alt="Débouchage wc par Haute pression">
                <p>Pour déboucher vos wc par la technique de haute pression, nos techniciens utilisent un jet propulsant de l'eau sous haute pression afin de disloquer le bouchon qui colmate la canalisation des toilettes</p>
				<a href="<?php echo $url; ?>step1.php" >Découvrir</a>
				<a href="tel: 0609151330" class="   fa-phone"> Appel d'Urgence</a>				
              </div>
                <div class="grid_4">
				<h3>Débouchage 100% Écologique</h3>
				<img src="../images/debouchage-100pour100-ecologique.png" alt="Mister Service Miami provide unclogging service at your home">
                <p>Le débouchage 100% écologique est réalisé SANS produits nocifs pour l'environnement et votre santé. Ce débouchage n'altère pas les canalisations car il est totalement dépourvu de produits corrosifs. </p>
				<a href="<?php echo $url; ?>step1.php" >Découvrir</a>
				<a href="tel: 0609151330" class="   fa-phone"> Appel d'Urgence</a>				
              </div> 
            </div>
						  <hr>
			            <div class="row off2 fadeInRight animated">

				<div class="grid_4">
					<h3> Tarif hydrocurage canalisations</h3>
					<p>Notre tarif hydrocurage canalisation est basé sur une intervention non complexe ou la canalisation est accessible par un regard de visite non condamné. Le forfait hydrocurage comprends 1H de main d'oeuvre + déplacement du véhicule</p>
					<table class="wow fadeInUp">
						  <tr>
							<td></td>
							<td>Tarif H.T</td>
						  </tr>
						  <tr>
							<td>Hydrocurage 30m<i class="fa fa-ellipsis-h" aria-hidden="true"></i></td>
							<td>Forfait 250€</td>
						  </tr>
						  <tr>
							<td>mètre linéaire suplémentaire<i class="fa fa-ellipsis-h" aria-hidden="true"></i></td>
							<td>10€ /m </td>
						  </tr>				  

						</table>
              </div>

			<div class="grid_4">
				<h3>Conditions tarifaires débouchage</h3>
               <ul>
<li>Hors taxes (HT):
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> particuliers = +10%,</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> professionnels = +20%</li></li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Notre prestation de "Débouchage manuel" / "débouchage domestique" comprend le débouchage wc,  à l'aide d'une pompe manuelle ou d'un furet. Le débouchage domestique ne comprends pas les prestations de curage et/ou hydrocurage des canalisations.</li>
<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Obligation du client : <br/>
Lors d’une prestation en hydrocurage (débouchage haute pression) : Mettre à disposition de notre technicien une arrivée d’eau.</li>
</ul>
              </div>
                 <div class="grid_3">
<section class="contact">
                    <div class=" f">
                        <div class="l"><img src="../images/contact-canaclean.jpg" alt=" - Contact"></div>
                        <div class=" r">
                            <div>
                                <p>Vous souhaitez une intervention d'urgence pour déboucher les wc?</p>
                                <div>
                                    <p>TÉLÉPHONEZ AU</p><a href="tel:"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="s">
                       <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                        <div class="r t-a-center"><a href="./contacts.php" class="btn btn-block ">NOUS CONTACTER</a></div>
                    </div>
                </section>
              </div>
            </div>
          </div>
        </section>


      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('../inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="../js/script-inner.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>