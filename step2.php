<?php
  include_once('php/global.php');
  include_once($INCLUDE . 'Product.inc.php');
  include_once($INCLUDE . 'Area.inc.php');
  include_once($INCLUDE . 'Panier.inc.php');
  session_start();
  //Products loading
  $pdts = array();
  $req = $bdd->query('SELECT * FROM products');
  while ( ($info = $req->fetch()) )
    array_push($pdts, new Product($info));
  //Area loading
  $areas = array();
  $req = $bdd->query('SELECT * FROM areas');
  while ( ($info = $req->fetch()) )
    array_push($areas, new Area($info));
?>
  <!DOCTYPE html>
  <html lang="fr">

  <head>
    <title>Tarifs Débouchage / Vidange / Fuite</title>
    <meta charset="utf-8">
    <meta name="description" content="Prix et tarifs Débouchage WC, débouchage canalisations">
    <?php   require('inc/meta.php') ?>
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/tarifs.css">
    <link rel="stylesheet" href="css/step.css">

    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="../images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="../js/html5shiv.js"></script><![endif]-->
    <script src="../js/device.min.js"></script>
  </head>

  <body>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=905277863011455";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      -->
      <header>
        <?php require('inc/menuheader.php'); ?>
      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins2 mobile-center">
          <div class="container">
            <div class="row ">
              <?php $_SESSION['panier']->getStep(2); ?>
              <br>

              <center><h2>Choisissez votre prestation à <?php echo $_SESSION['panier']->getArea()->getName(); ?> : </h2></center><br>
              <div class="col-sm-9 col-lg-9">
                <?php foreach ($pdts as $pdt) { ?>
                <div class="col-sm-5 col-lg-5">
                  <div id="box">
                    <div id="cadre_desc">
                      <img src="<?php echo $pdt->getImgLink(); ?>" alt="img" />
                      <div class="center_prix"><a style="font-size:18px">À partir de</a> <br>
                        <?php echo $pdt->getPriceHT($_SESSION['panier']->getArea()->getNumZone() ); ?>€</div>
                    </div>
                    <div class="center_desc">
                      <div class="titre">
                        <?php echo $pdt->getName(); ?>
                      </div>
                      <div class="desc">
                        <?php echo $pdt->getDesc(); ?>
                      </div>
                      <br>
                      <form class="" action="step3.php" method="post">
                      <button type="submit" name="id" value="<?php echo $pdt->getId(); ?>" class="btn" readonly>En savoir plus</button>
                      </form>
                    </div>

                  </div>
                </div>
                <?php } ?>
              </div>




              <!--<?php foreach ($pdts as $pdt) { ?>
								<section class="tarif">
									<div class="row">
										<div class="separator"></div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 tarif-denom">
                      <form class="" action="step3.php" method="post">
                        <button type="submit" name="id" value="<?php echo $pdt->getId(); ?>" readonly><?php echo $pdt->getName(); ?></button>
                      </form>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 tarif-denom">
                      <p><?php echo $pdt->getDesc(); ?></p>
                      <img src="<?php echo $pdt->getImgLink(); ?>" alt="img"/>

                    </div>
										<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prix"><?php echo $pdt->getPriceHT(); ?>€</div>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prix">Voir la fiche</div>
									</div>
								</section>
                  <?php } ?>-->
              <div class="col-sm-4 col-lg-3">
                <section class="contact">
                  <div class=" f">
                    <div class="l"><img src="images/contacter-canaclean.jpg" alt=" - Contact" class="img-responsive"></div>
                    <div class=" r">
                      <div>
                        <p>Vous souhaitez une intervention d'urgence pour le débouchage d'une canalisation</p>
                        <div>
                          <p>CONTACTEZ-NOUS AU</p><a href="tel:" class="btn btn-block"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                      </div>
                    </div>
                  </div>
                  <div class="s">
                    <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                    <div class="r t-a-center"><a href="contacts.php" class="btn btn-block"><i class="fa-comments"></i> NOUS CONTACTER </a></div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
        </section>
        <hr>
        <div class="container">
          <ul class="row product-list">
            <li class="grid_6">
              <div class="box wow fadeInRight">
                <div class="box_aside">
                  <div class="icon fa-comments"></div>
                </div>
                <div class="box_cnt__no-flow">
                  <h3><a href="#">A votre service</a></h3>
                  <p>Réflechir avant d'agir. Vos problème de wc bouché, de canalisation colmatée, de fuite d'eau doivent être
                    solutionnés au plus vite, mais pas n'importe comment et pas à n'importe quel prix!!!</p>
                </div>
              </div>
              <hr>
              <div data-wow-delay="0.2s" class="box wow fadeInRight">
                <div class="box_aside">
                  <div class="icon fa-calendar-o"></div>
                </div>
                <div class="box_cnt__no-flow">
                  <h3><a href="#">Sur rendez-vous ou en urgence</a></h3>
                  <p>Plannifiez un hydrocurage des canalisations, un détartrage de votre tuyauterie, ou appellez nous en urgence
                    pour une recherche de fuite, tout est possible, nous intervenons tous les jours de la semaine sur Bordeaux
                    Métrople et dans toute la Gironde</p>
                </div>
              </div>
            </li>
            <li class="grid_6">
              <div data-wow-delay="0.3s" class="box wow fadeInRight">
                <div class="box_aside">
                  <div class="icon fa-group"></div>
                </div>
                <div class="box_cnt__no-flow">
                  <h3><a href="#">Une équipe de professionnels</a></h3>
                  <p>Une véritable équipe de professionnels ayant plusieurs années d'expérience ça compte énormément. Chaque
                    chantier est unique, mais l'expérience ça aide toujours.</p>
                </div>
              </div>
              <hr>
              <div data-wow-delay="0.4s" class="box wow fadeInRight">
                <div class="box_aside">
                  <div class="icon fa-thumbs-up"></div>
                </div>
                <div class="box_cnt__no-flow">
                  <h3><a href="#">Service de qualité</a></h3>
                  <p>Qualité et efficacité font partie de l'adn de débouchage-bordeaux.fr. Pas de surprise, nous intervenons
                    pour déboucher vos toilettes, hydrocurer les canalisations ou rechercher une fuite. Et le tarif, nous
                    l'annonçons au début de l'intervention, pas à la fin.</p>
                </div>
              </div>
            </li>
          </ul>
          <br>
        </div>
    </div>
    </section>

    </main>
    <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
    <footer>
      <?php require('inc/menufooter.php'); ?>

    </footer>
    </div>
    <script src="js/script-inner.js"></script>
    <script>
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
          m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

      ga('create', 'UA-70918441-6', 'auto');
      ga('send', 'pageview');
    </script>
  </body>

  </html>
