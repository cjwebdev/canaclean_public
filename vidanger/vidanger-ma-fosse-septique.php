<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Vidanger ma fosse septique</title>
    <meta charset="utf-8">
	
	<meta name="description" content="Canaclean réalise la vidange de votre fosse septique">
	<?php   require('../inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/grid.css">
    <link rel="stylesheet" href="../css/style.css">
<link rel="stylesheet" href="../css/tarifs.css">	
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="../images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="../js/html5shiv.js"></script><![endif]-->
    <script src="../js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
          <?php require('../inc/menuheader-inner.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins4 bg-image-vidange-fosse-septique">
          <div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Vidanger ma fosse septique</h2>
                <p>Les particuliers possédant une fosse septique sont dans l'obligation réglementaire de procéder à l'entretien régulier de celle-ci.  Que ce soit une fosse septique, une fosse toutes eaux ou encore une microstation, la vidange doit être réalisée en fonction de la hauteur des boues de décantation.</p>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>Pompage des boues de décantation</li>
                      <li>Nettoyage haute pression</li>					  
                      <li>Vérification de l'état de la fosse</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>Vidange de la fosse</li>
                      <li>Remise en eaux</li>
                      <li>Traitement des déchets</li>

                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>	  
<section class="well1 ins2 mobile-center">
          <div class="container">
            <div class="row off2 fadeInRight animated">

				<div class="grid_4">
					<h3> Tarif vidange de fosse septique</h3>
					<p>Canaclean vous donne le choix entre 3 forfaits complets (pompage des graisses, nettoyage haute pression, vidange, traitement des déchets...) en fonction du volume de graisse. Moins d'1m3, entre 1 et 2m3, au delà de 2m3. </p>
					<table class="wow fadeInUp">
						  <tr>
							<td></td>
							<td>Tarif H.T</td>
						  </tr>
						  <tr>
							<td>Vidange fosse septique 
								<br/>Moins d'1m3</td>
							<td>à partir de <br/>200 €</td>
						  </tr>
						  <tr>
							<td>Vidange bac à graisse<br/> 1m3 < < 2m3 </td>
							<td>à partir de <br/>300 €</td>
						  </tr>
						  <tr>
							<td>Vidange bac à graisse<br/> au delà de 2m3</td>
							<td>à partir de <br/>400 €</td>
						  </tr>						  
						  <tr>
							<td>Contrat annuel Vidange bac à graisse</td>
							<td>sur devis</td>
						  </tr>				  
						  <tr>
							<td>Installation d'un bac à graisse</td>
							<td>sur devis</td>
						  </tr>
						</table>
						<ul>
							<li>particuliers : TVA = 20%</li>
							<li>professionnels : TVA = 10%</li>
						</ul>
              </div>
			<div class="grid_4">
                <h3>Particuliers & Professionnels</h3>				
				<img src="../images/grease-trap.jpg"   alt="Mister Service Miami Pump grease trap">
                <p>Particuliers ou professionnels, Canaclean intervient partout en Gironde pour la vidange de bac à graisse. Pour votre confort et votre tranquilité, nous proposons aussi des contrats d'entretien annuel prévoyant plusieurs passages à déterminer ensemble. Vous serez gagnants et tranquilles.</p>
              </div>
                 <div class="grid_3">
<section class="contact">
                    <div class=" f">
                        <div class="l"><img src="../images/contact-canaclean.jpg" alt=" - Contact"></div>
                        <div class=" r">
                            <div>
                                <p>Vous souhaitez une <b>intervention d'urgence</b> pour le débouchage d'une canalisation ?</p>
                                <div>
                                    <p>CONTACTEZ-NOUS AU</p><a href="tel:"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="s">
                       <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                        <div class="r t-a-center"><a href="./contacts.php" class="btn btn-block ">NOUS CONTACTER</a></div>
                    </div>
                </section>
</div>
            </div>
		<hr>
            <h2>Bac à graisse, les informations à savoir</h2> 
			<div class="row fadeInRight animated">
              <div class="grid_4"><img src="../images/utilite-bac-a-graisse.jpg" alt="Intérêt du bac à graisse">
                <h3>Fosse septique ou fosse toutes eaux: quelle différence?</h3>
                <p>La fosse septique ne traite que les eaux en provenance des toilettes. Ces eaux sont appellées "eaux vannes". LA fosse toutes eaux traite l'ensemble des eaux usées de votre habitation. La fosse septique n'est plus autorisée lors d'une construction mais reste tolérée lors d'une rénovation sous condition d'aménagement (couplée par exemple à un bac à graisse). Il faudra cependant réaliser des travaux de mise aux normes. Attention, le terme "fosse septique" étant entré dans le langage courant, il peut tout aussi bien désigner une fosse septique, ou une fosse toutes eaux et donc engendrer des incompréhensions.<./p>
				
              </div>
              <div class="grid_4"><img src="../images/fosse-septique-fonctionnement.jpg" alt="Fonctionnement d'une fosse septique simple">
                <h3>Fonctionnement de la fosse septique</h3>
                <p>Le bac à graisse permet la récupération des graisses présentes dans les eaux usées afin de renvoyer une eau claire dans le réseau d’assainissement, par principe de décantation. Une couche de graisse va se former à la surface du bac tandis que la fécule (essentiellement des restes alimentaires) va se déposer au fond du bac. Au fil du temps, la couche de graisse et la couche de fécule augmentent et il est nécessaire de faire vidanger et nettoyer le bac pour continuer à rejeter une eau claire.</p>
              </div>
              <div class="grid_4"><img src="../images/hydrocurage-bacagraisse.jpg" alt="Nettoyage haute pression du bac à graisse">
                <h3>Nettoyage haute pression</h3>
                <p>Après le pompage des graisses et des boues de décantation, les techniciens Canaclean procédent au nettoyage haute pression des parois ainsi qu'aux canalisations d'entrée et sortie du bac à graisse. Dans le cas d'une cuisine de restauration collective, ils réalisent également le nettoyage haute pression des siphons de sols pour éviter tout colmatage des canalisations en direction du bac à graisse.</p>
              </div>
            </div>
          </div>
        </section>     
      </main>
				<!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('../inc/menufooter.php')?> 

      </footer>
    </div>
    <script src="../js/script-inner.js"></script>

  </body>
</html>