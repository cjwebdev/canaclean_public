<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Vidange bac à graisse</title>
    <meta charset="utf-8">
	
	<meta name="description" content="Pour vidanger mon bac à graisse, je recommande Canaclean. Vidange du bac à graisse à partir de 200€ à Bordeaux et dans toute la Gironde au 05 33 08 08 08">
	<?php   require('../inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/grid.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/tarifs.css">	
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="../images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="../js/html5shiv.js"></script><![endif]-->
    <script src="../js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
          <?php require('../inc/menuheader-inner.php')?> 

      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins4 bg-image">
			<div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Vidanger mon bac à graisse</h2>
                <p>Pour continuer à rejeter des eaux claires dans votre fosse septique il est nécessaire de vidanger le bac à graisse régulièrement. La fréquence de vidange de bac à graisse est determinée par le volume du bac et son utilisation. Si vous êtes un particulier, son utilisation sera moins intensive qu'un professionnel de la restauration. En fonction de sa capacité un bac à graisse doit être vidangé  de une à trois fois par an en moyenne.
				 
					</p><h3>Notre prestation vidange de bac à graisse comprends l'ensemble des points suivants:</h3>
                <div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>Pompage des graisses</li>
                      <li>Nettoyage haute pression du bac</li>					  	
                      <li>Vidange du bac à graisse</li>
                      <li>Remise en eau</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>Hydrocurage des canalisations</li>
                      <li>Nettoyage des siphons de sol</li>
                      <li>Traitement des déchets</li>
                      <li>Vérification du bac à graisse</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
 <section class="well1 ins2 mobile-center">
          <div class="container">
            <div class="row off2 fadeInRight animated">

				<div class="grid_4">
					<h3> Tarif vidange bac à graisse</h3>
					<p>Canaclean vous donne le choix entre 3 forfaits complets (pompage des graisses, nettoyage haute pression, vidange, traitement des déchets...) en fonction du volume de graisse. Moins d'1m3, entre 1 et 2m3, au delà de 2m3. </p>
					<table class="wow fadeInUp">
						  <tr>
							<td></td>
							<td>Tarif H.T</td>
						  </tr>
						  <tr>
							<td>Vidange bac à graisse 
								<br/>Moins d'1m3</td>
							<td>à partir de <br/>200 €</td>
						  </tr>
						  <tr>
							<td>Vidange bac à graisse<br/> 1m3 < < 2m3 </td>
							<td>à partir de <br/>300 €</td>
						  </tr>
						  <tr>
							<td>Vidange bac à graisse<br/> au delà de 2m3</td>
							<td>à partir de <br/>400 €</td>
						  </tr>						  
						  <tr>
							<td>Contrat annuel Vidange bac à graisse</td>
							<td>sur devis</td>
						  </tr>				  
						  <tr>
							<td>Installation d'un bac à graisse</td>
							<td>sur devis</td>
						  </tr>
						</table>
						<ul>
							<li>particuliers : TVA = 20%</li>
							<li>professionnels : TVA = 10%</li>
						</ul>
              </div>
			<div class="grid_4">
                <h3>Particuliers & Professionnels</h3>				
				<img src="../images/bac-a-graisse.jpg"   alt="Canaclean vidange mon bac à graisse">
                <p>Particuliers ou professionnels, Canaclean intervient partout en Gironde pour la vidange de bac à graisse. Pour votre confort et votre tranquilité, nous proposons aussi des contrats d'entretien annuel prévoyant plusieurs passages à déterminer ensemble. Vous serez gagnants et tranquilles.</p>
              </div>
                 <div class="grid_3">
<section class="contact">
                    <div class=" f">
                        <div class="l"><img src="../images/contact-canaclean.jpg" alt="Canaclean - Contact"></div>
                        <div class=" r">
                            <div>
                                <p>Vous souhaitez une <b>intervention d'urgence</b> pour le débouchage d'une canalisation ?</p>
                                <div>
                                    <p>CONTACTEZ-NOUS AU</p><a href="tel:"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="s">
                       <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                        <div class="r t-a-center"><a href="./contacts.php" class="btn btn-block ">NOUS CONTACTER</a></div>
                    </div>
                </section>
              </div>
            </div>
		<hr>
            <h2>Bac à graisse, les informations à savoir</h2> 
			<div class="row fadeInRight animated">
              <div class="grid_4"><img src="../images/utilite-bac-a-graisse.jpg" alt="Intérêt du bac à graisse">
                <h3>Utilité du bac à graisse</h3>
                <p>Le bac à graisse a pour mission la collecte des graisses contenues dans les eaux usées en provenance des éviers, salles de bains, machine à laver le linge et machine à laver la vaisselle. Son objectif est de décharger les eaux usées de leur graisse avant leur passage dans la fosse septique ou le réseau d'assainissement. Il est obligatoire pour les installations de cuisine professionnelles et collectives. Le bac à graisse doit être installé entre votre cuisine et la fosse septique.</p>
				
              </div>
              <div class="grid_4"><img src="../images/fonctionnement-bac-a-graisse.jpg" alt="Fonctionnement du bac à graisse">
                <h3>Fonctionnement du bac à graisse</h3>
                <p>Le bac à graisse permet la récupération des graisses présentes dans les eaux usées afin de renvoyer une eau claire dans le réseau d’assainissement, par principe de décantation. Une couche de graisse va se former à la surface du bac tandis que la fécule (essentiellement des restes alimentaires) va se déposer au fond du bac. Au fil du temps, la couche de graisse et la couche de fécule augmentent et il est nécessaire de faire vidanger et nettoyer le bac pour continuer à rejeter une eau claire.</p>
              </div>
              <div class="grid_4"><img src="../images/hydrocurage-bacagraisse.jpg" alt="Nettoyage haute pression du bac à graisse">
                <h3>Nettoyage haute pression</h3>
                <p>Après le pompage des graisses et des boues de décantation, les techniciens Canaclean procédent au nettoyage haute pression des parois ainsi qu'aux canalisations d'entrée et sortie du bac à graisse. Dans le cas d'une cuisine de restauration collective, ils réalisent également le nettoyage haute pression des siphons de sols pour éviter tout colmatage des canalisations en direction du bac à graisse.</p>
              </div>
            </div>
          </div>
        </section>
 

      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('../inc/menufooter-inner.php')?> 

      </footer>
    </div>
    <script src="../js/script-inner.js"></script>

  </body>
</html>