<?php
/*
**
**  After step1
**
*/
  include_once('../php/global.php');
  include_once($INCLUDE . 'Product.inc.php');
  include_once($INCLUDE . 'Area.inc.php');
  include_once($INCLUDE . 'Panier.inc.php');
  session_start();

  //Check eligibility of Code postal
  if (isset($_GET['cp'])
  && isset($_GET['name']))
  {
    $req = $bdd->prepare('SELECT * FROM areas WHERE cp = :cp AND name = :name');
    $req->execute(array(
      'cp' => $_GET['cp'],
      'name' => $_GET['name']
    ));
    if( !($info = $req->fetch()) ) {
      header('Location: ../step1.php?eligible=no');
      exit;
    }
    print_r($info);
    $_SESSION['panier'] = new Panier();
    echo $_info['num_zone'];
    $_SESSION['panier']->setArea(new Area($info));
    header('Location: ../step2.php');
    exit;
  }
  header('Location: ../step1.php?eligible=no');
  exit;
?>
