<?php
  include_once('../php/global.php');
  include_once($INCLUDE . 'Product.inc.php');
  include_once($INCLUDE . 'Area.inc.php');
  include_once($INCLUDE . 'Panier.inc.php');

  //Area loading
  $areas = array();
  $req = $bdd->query('SELECT * FROM areas');
  $res = array();
  while ( ($info = $req->fetch()) )
  {
    array_push($res, $info);
  }
  header('Content-Type: application/json');
  echo json_encode($res);
?>
