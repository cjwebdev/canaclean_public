<?php
  include_once('../php/global.php');
  include_once($INCLUDE . 'Product.inc.php');
  include_once($INCLUDE . 'Area.inc.php');
  include_once($INCLUDE . 'AddrPro.inc.php');
  include_once($INCLUDE . 'AddrPart.inc.php');
  include_once($INCLUDE . 'Panier.inc.php');

  session_start();

  function particular($panier)
  {
    foreach ($_POST as $k => $one) {
      $_POST[$k] = htmlspecialchars($_POST[$k]);
    }
    $addr = new AddrPart($_POST);
    $panier->setAddress($addr);
    $panier->setAddrType(0); //part
  }

  function professionnal($panier)
  {
    foreach ($_POST as $k => $one) {
      $_POST[$k] = htmlspecialchars($_POST[$k]);
    }
    $addr = new AddrPro($_POST);
    $panier->setAddress($addr);
    $panier->setAddrType(1); //pro
  }

  if(isset($_POST['statut']))
  {
    $panier = $_SESSION['panier'];
    if (strcmp($_POST['statut'], "part") == 0)
      particular($panier);
    else if (strcmp($_POST['statut'], "pro") == 0)
      professionnal($panier);
  }
  else{
    header('Location: ../index.php');
    exit;
  }
  header('Location: ../step5.php');
  exit;
?>
