
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Contacts</title>
    <meta charset="utf-8">
	<meta name="description" content="Restons en contact au 05 33 08 08 08 ! Canaclean vous assure une intervention  pour le débouchage wc, débouchage canalisations à Bordeaux et sur l'ensemble de la Gironde du lundi au dimanche sur simple appel téléphonique">
	<meta name="keyword" content="contact débouchage arcachon, contact débouchage wc arcachon, contact pour déboucher wc arcachon, débouche wc arcachon ">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
	<?php   require('inc/meta.php') ?> 
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/google-map.css">
    <link rel="stylesheet" href="css/mailform.css">
    <link rel="stylesheet" href="css/tarifs.css">	
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="Vous utilisez un navigateur internet obsolète (Internet Explorer). Pour une meilleure expérience, téléchargez une navigateur récent de type Chrome, Firefox, Edge, Opéra ou Safari"></a></div>
    </html>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div class="page">
      <!--
      ========================================================
      							HEADER
      ========================================================
      
      
      -->
      <header>
		<?php $nav_en_cours  = 'contacts' ?>
        <?php   require('inc/menuheader.php') ?> 
      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
	  		  <?php
if (isset($_POST) && isset($_POST['nom']) && isset($_POST['telephone']) && isset($_POST['email']) && isset($_POST['message']))
{
    include("inc/checkform.php");
}
?>
         <section class="well3 bg-secondary2">
          <div class="container">
		<div class="row">
              <div class="grid_3 ">
                <section class="contact">
                    <div class=" f">
                        <div class="l"><img src="images/contact-canaclean.jpg" alt=" - Contact"></div>
                        <div class=" r">
                            <div>
                                <p>Vous souhaitez une <b>intervention d'urgence</b> pour le débouchage d'une canalisation ?</p>
                                <div>
                                    <p>CONTACTEZ-NOUS AU</p><a href="tel:"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="s">
                       <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                        <div class="r t-a-center"><a href="./contacts.php" class="btn btn-block ">NOUS CONTACTER</a></div>
                    </div>
                </section>
          </div>		
		<div class="grid_8">

            <h2 id="myform">Devis, Rendez-vous, Information</h2>
		   <form method="post" action="contacts.php"  class="mailform off2">
				
				<p>Laissez nous votre message, nous vous répondrons dans les plus brefs délais<br/><i>Tous les champs marqués par un * sont obligatoires</i></p>
              <fieldset class="row">
				<label class="grid_4" for="inter">
					<select id="contact_prestation" name="inter" required="required" class="input-lg form-control" aria-required="true">
						<option value="" selected="selected">TYPE D'INTERVENTION <i class="fa-caret-down" aria-hidden="true"></i></option>
						<option value="debouchage-wc">Débouchage wc</option>
						<option value="debouchage-canalisation">Débouchage canalisation</option>
						<option value="dréacinnage">Déraçinnage canalisation</option>
						<option value="curage-ep-eu">Curage réseau eu, ep</option>
						<option value="recherche-fuite">Recherche de fuite</option>
						<option value="passage-caméra">Inspection vidéo canalisation</option>
						<option value="vidange-fosse-septique">Vidange de fosse septique</option>
						<option value="vidange-bac-graisse">Vidange bac à graisse</option>
						<option value="vidange-anc-autre">Vidange assainissement autre</option>
						<option value="pompage-eau-claires">Pompage eaux claires</option>
						<option value="pompage-eaux-chargée">Pompage eaux chargées</option>
						
					</select>
				</label>
                <label class="grid_4" for="statut">
					<select placeholder="Vous êtes : *" name="statut">
					<option value="" selected="selected">Vous êtes <i class="fa fa-caret-down" aria-hidden="true"></i></option>
					<option value="particulier-locataire">particulier-locataire</option>
					<option value="particulier-propriétaire">particulier-propriétaire</option>
					<option value="syndic">syndic</option>
					<option value="société">société</option>
				  </select>
				  </label>
				  <label class="grid_4" for="name">
                  <input type="text"  placeholder="Votre Société/Syndic"  name="nom-societe"  >
                </label>
                <label class="grid_4" for="name">
                  <input type="text"  placeholder="Votre NOM : *"  name="nom"  >
                </label>
                <label class="grid_4" for="telephone">
                  <input type="text"  placeholder="Telephone : *" name="telephone" >
                </label>
                <label class="grid_4" for="email">
                  <input type="text"  placeholder="Email : *" name="email">
                </label>
                <label class="grid_8" for="message">
                  <textarea  placeholder="Merci de détailler votre demande : *" name="message"></textarea>
                </label>
                <div class="mfControls grid_4">
                  <button type="submit" class="btn">Envoyer</button>
                </div>
              </fieldset>
            </form>
			</div>
              			
			</div>
          </div>
	        </section>
       <section class="map">
          <div id="map" class="map_model"></div>
          
			
        </section>		

      </main>
      <!--
      ========================================================
                                  FOOTER
      ========================================================
      -->
      <footer>
        <?php require('inc/menufooter.php')?> 
      </footer>
    </div>
    <script src="js/script.js"></script>
	<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDV5UklTpQO4apEm8S5_LIdjNHeeEVJIkQ&callback=initMap">
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70918441-6', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>