<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include_once('php/global.php');
include_once('pdf/fpdf.php');
include_once($INCLUDE . 'Product.inc.php');
include_once($INCLUDE . 'Panier.inc.php');
include_once($INCLUDE . 'Area.inc.php');
include_once($INCLUDE . 'Option.inc.php');
session_start();
$panier = $_SESSION['panier'];
if(!$panier->getProduct()){
  header('Location: /');
}
$price = $panier->getProduct()->getPriceHT($panier->getArea()->getNumZone());
$prix_eligible = ($price + ($panier->getArea()->getMajor() / 100) * $price);
$product = $panier->getProduct();
$area = $panier->getArea();

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <title>Contacts</title>
  <meta charset="utf-8">
  <meta name="description" content="Restons en contact au 05 33 08 08 08 ! Canaclean vous assure une intervention  pour le débouchage wc, débouchage canalisations à Bordeaux et sur l'ensemble de la Gironde du lundi au dimanche sur simple appel téléphonique">
  <meta name="keyword" content="contact débouchage arcachon, contact débouchage wc arcachon, contact pour déboucher wc arcachon, débouche wc arcachon ">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <?php   require('inc/meta.php') ?>
  <meta name="format-detection" content="telephone=no">
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/grid.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/google-map.css">
  <link rel="stylesheet" href="css/mailform.css">
  <link rel="stylesheet" href="css/tarifs.css">
  <link rel="stylesheet" href="css/step.css">
  <link rel="stylesheet" href="css/pdf.css">

  <script src="js/jquery.js"></script>
  <script src="js/jquery-migrate-1.2.1.js"></script>
  <!--[if lt IE 9]>
  <html class="lt-ie9">
  <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="Vous utilisez un navigateur internet obsolète (Internet Explorer). Pour une meilleure expérience, téléchargez une navigateur récent de type Chrome, Firefox, Edge, Opéra ou Safari"></a></div>
  </html>
  <script src="js/html5shiv.js"></script><![endif]-->
  <script src="js/device.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/PayzenJS@1.0.5/payzenjs.js"></script>
</head>

<body>  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=905277863011455";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <div class="page">
    <!--
    ========================================================
    HEADER
    ========================================================


  -->
  <header>
  				<div class="container">
          <div class="brand">
            <h1 class="brand_name"><a href="index.php"><img src="images/canaclean-logo-331.png" alt"LOGO débouchage bordeaux.fr" /></a></h1>
           </div>
			<div id="stuck2" class=" brandtoo">
				<ul class="brandtoo">
				<li ><a href="contacts.php" ><i class="fa-comments"></i> Contact</a></li>
				<li ><a href="tel:0533080808" ><i class="fa-phone big"></i> <span class="txtsmll">0533080808</span></a></li>
				<li ><a href="tarifs/prix-prestation-debouchage-vidange.php" ><i class="fa-tags"></i> Tarifs</a></li>
				<li class="db-big">
					<a href="contacts.php#tarifs-debouchage" >
						<i class="fa fa-life-ring" aria-hidden="true"></i><br/>
						<span class="txt1">URGENCE</span><br/>
						<span class="txt2">DEBOUCHAGE</span><br/>
						<span class="txt3"> 06 09 15 13 30</span></a></li>
			</ul>
			</div>
        </div>
        <div id="stuck_container" class="stuck_container">
          <div class="container">

        <nav class="nav">
              <ul data-type="navbar" class="sf-menu">
                <li><a href="./merci.php">Retourner sur Canaclean</a></li>
            </nav>
          </div>
        </div>
  </header>
  <!--
  ========================================================
  CONTENT
  ========================================================
-->
<main>

<style>
IFRAME{
  width: 100%;
}
</style>

  <section class="well3 bg-secondary2">
    <div class="container">
      <div class="row">

        <div class="grid_12">
            <div id="paiement"/>
          </div>
          </div>
        </div>
      </section>
      <br>
      <div class="container">
        <ul class="row product-list">
          <li class="grid_6">
            <div class="box wow fadeInRight">
              <div class="box_aside">
                <div class="icon fa-comments"></div>
              </div>
              <div class="box_cnt__no-flow">
                <h3><a href="#">A votre service</a></h3>
                <p>Réflechir avant d'agir. Vos problème de wc bouché, de canalisation colmatée, de fuite d'eau doivent être
                  solutionnés au plus vite, mais pas n'importe comment et pas à n'importe quel prix!!!</p>
                </div>
              </div>
              <hr>
              <div data-wow-delay="0.2s" class="box wow fadeInRight">
                <div class="box_aside">
                  <div class="icon fa-calendar-o"></div>
                </div>
                <div class="box_cnt__no-flow">
                  <h3><a href="#">Sur rendez-vous ou en urgence</a></h3>
                  <p>Plannifiez un hydrocurage des canalisations, un détartrage de votre tuyauterie, ou appellez nous en urgence
                    pour une recherche de fuite, tout est possible, nous intervenons tous les jours de la semaine sur Bordeaux
                    Métrople et dans toute la Gironde</p>
                  </div>
                </div>
              </li>
              <li class="grid_6">
                <div data-wow-delay="0.3s" class="box wow fadeInRight">
                  <div class="box_aside">
                    <div class="icon fa-group"></div>
                  </div>
                  <div class="box_cnt__no-flow">
                    <h3><a href="#">Une équipe de professionnels</a></h3>
                    <p>Une véritable équipe de professionnels ayant plusieurs années d'expérience ça compte énormément. Chaque
                      chantier est unique, mais l'expérience ça aide toujours.</p>
                    </div>
                  </div>
                  <hr>
                  <div data-wow-delay="0.4s" class="box wow fadeInRight">
                    <div class="box_aside">
                      <div class="icon fa-thumbs-up"></div>
                    </div>
                    <div class="box_cnt__no-flow">
                      <h3><a href="#">Service de qualité</a></h3>
                      <p>Qualité et efficacité font partie de l'adn de débouchage-bordeaux.fr. Pas de surprise, nous intervenons
                        pour déboucher vos toilettes, hydrocurer les canalisations ou rechercher une fuite. Et le tarif, nous
                        l'annonçons au début de l'intervention, pas à la fin.</p>
                      </div>
                    </div>
                    <br>
                  </li>

                </ul>
              </div>

            </main>
            <!--
            ========================================================
            FOOTER
            ========================================================
          -->
          <footer>
            <?php require('inc/menufooter.php'); ?>
          </footer>
        </div>
      </body>
      <script type="text/javascript">
          $( document ).ready(function() {
            console.log('ntm');
            PayzenJS.go({
              canvas: {
                id: "paiement"
              },
              orderData: {
                vads_site_id: "54664749", //id boutique canaclean:40555726 id cj:54664749
                vads_ctx_mode: "TEST",
                vads_amount: "<?php echo $panier->getTotalTTC() * 100; ?>",
                vads_trans_id: "<?PHP echo rand(100000, 700000); ?>",
                vads_cust_email: "<?php echo $panier->getAddress()->getMail(); ?>",
                vads_currency: "978",
                vads_trans_date: "<?PHP echo date("YmdHis"); ?>",
                vads_action_mode: "INTERACTIVE",
                vads_page_action: "PAYMENT",
                vads_version: "V2",
                vads_payment_config: "SINGLE",
                vads_validation_mode: "0",
                vads_capture_delay: "0",
                vads_return_mode: "POST",
                vads_redirect_success_timeout: '10',
                vads_url_return: 'https://localhost/canaclean/',
                vads_redirect_success_message: 'Redirection vers la boutique dans quelques instants',
                vads_redirect_error_timeout: "1",
                vads_redirect_error_message: "Redirection vers la boutique dans quelques instants",
                vads_cust_id: "<?php echo $panier->getIdCommandOnDb(); ?>"
                              },
              credentials : {
                source: "php/payzen.php"
              }
            });
          });
        </script>
<?php 
// $panier->clean();
?>

      </html>
