function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function validatePhone(nb) {
  var re = /^(0[1-68])(?:[ _.-]?(\d{2})){4}$/;
  return re.test(nb);
}
function validateNumber(nb) {
  var re = /^[a-zA-Z]+$/;
  return !re.test(nb);
}
function validateCp(cp) {
  var re = /^[a-zA-Z]+$/;
  return cp.length == 5 && !re.test(cp);
}
function validateSiret(obj) {
  var re = /^[a-zA-Z]+$/;
  return obj.length == 14 && !re.test(obj);
}
function checkCp(obj, e) {
  if (!validateCp(obj.val())) {
    obj.parent().find('.inpErr').css('display', 'block');
    obj.parent().find('.inpErr').find('h7').text('Code postal inconnu.');
    e.preventDefault();
    return true;
  }
  obj.parent().find('.inpErr').css('display', 'none');
  return false;
}
function checkVoid(obj, e) {
  if (obj.val() == "") {
    obj.parent().find('.inpErr').css('display', 'block');
    obj.parent().find('.inpErr').find('h7').text('Merci de remplir ce champ');
    e.preventDefault();
    return true;
  }
  obj.parent().find('.inpErr').css('display', 'none');
  return false;
}
function checkNum(obj, e) {
  if (!validateNumber(obj.val())) {
    obj.parent().find('.inpErr').css('display', 'block');
    obj.parent().find('.inpErr').find('h7').text('Ne peut contenir que des numeros');
    e.preventDefault();
    return true;
  }
  obj.parent().find('.inpErr').css('display', 'none');
  return false;
}

function checkTel(obj, e) {
  if (!validatePhone(obj.val())) {
    obj.parent().find('.inpErr').css('display', 'block');
    obj.parent().find('.inpErr').find('h7').text('Telephone non valide');
    e.preventDefault();
    return true;
  }
  obj.parent().find('.inpErr').css('display', 'none');
  return false;
}
function checkMail(obj, e) {
  if (!validateEmail(obj.val())) {
    obj.parent().find('.inpErr').css('display', 'block');
    obj.parent().find('.inpErr').find('h7').text('Format incorrect');
    e.preventDefault();
    return true;
  }
  obj.parent().find('.inpErr').css('display', 'none');
  return false;
}
function checkSiret(obj) {
  if (!validateSiret(obj.val())) {
    obj.parent().find('.inpErr').css('display', 'block');
    obj.parent().find('.inpErr').find('h7').text('Format incorrect');
    e.preventDefault();
    return true;
  }
  obj.parent().find('.inpErr').css('display', 'none');
  return false;
}
$(document).ready(function () {
  $('#part_form').css('display', 'block');
  $('#pro_form').css('display', 'none');
  $('#part_radio').click(function () {
    $('#part_form').css('display', 'block');
    $('#pro_form').css('display', 'none');
  });
  $('#pro_radio').click(function () {
    $('#pro_form').css('display', 'block');
    $('#part_form').css('display', 'none');
  });
  //Check validity of #addrFomr
  $('#addrForm').submit(function (e) {
    //Check Addr
    if (!checkVoid($('input[name="mail"]'), e))
      checkMail($('input[name="mail"]'), e);
    if (!checkVoid($('input[name="tel"]'), e))
      checkTel($('input[name="tel"]'), e);
    if (!checkVoid($('input[name="fNumRoad"]'), e))
      checkNum($('input[name="fNumRoad"]'), e);
    if (!checkVoid($('input[name="numRoad"]'), e))
      checkNum($('input[name="numRoad"]'), e);
    if (!checkVoid($('input[name="fCp"]'), e))
      checkCp($('input[name="fCp"]'), e);
    checkVoid($('input[name="addr"]'), e);
    checkVoid($('input[name="fAddr"]'), e);
    checkVoid($('input[name="name"]'), e);
    checkVoid($('input[name="surname"]'), e);
    checkVoid($('input[name="fVille"]'), e);
    //Check part
    if ($('#part_radio').is(':checked')) {
      var obj = $('select[name="locataire"]');
      if (obj.val() == null) {
        obj.parent().find('.inpErr').css('display', 'block');
        obj.parent().find('.inpErr').find('h7').text('Merci de renseigner votre situation');
        e.preventDefault();
      }
      else
        obj.parent().find('.inpErr').css('display', 'none');
    }
    else //Check pro
    {
      if (!checkVoid($('input[name="siret"]'), e))
        checkSiret($('input[name="siret"]'), e);
      checkVoid($('input[name="statutEntp"]'), e);
      checkVoid($('input[name="nameEntp"]'), e);
    }
  });
});
