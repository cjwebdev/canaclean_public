<?php
include_once('php/global.php');
include_once($INCLUDE . 'Product.inc.php');
include_once($INCLUDE . 'Panier.inc.php');
include_once($INCLUDE . 'Area.inc.php');
include_once($INCLUDE . 'Option.inc.php');
session_start();
$panier = $_SESSION['panier'];
if(!$panier->getProduct()){
  header('Location: /');
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Contacts</title>
  <meta charset="utf-8">
  <meta name="description" content="Restons en contact au 05 33 08 08 08 ! Canaclean vous assure une intervention  pour le débouchage wc, débouchage canalisations à Bordeaux et sur l'ensemble de la Gironde du lundi au dimanche sur simple appel téléphonique">
  <meta name="keyword" content="contact débouchage arcachon, contact débouchage wc arcachon, contact pour déboucher wc arcachon, débouche wc arcachon ">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <?php   require('inc/meta.php') ?>
  <meta name="format-detection" content="telephone=no">
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/grid.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/google-map.css">
  <link rel="stylesheet" href="css/mailform.css">
  <link rel="stylesheet" href="css/tarifs.css">
  <link rel="stylesheet" href="css/step4.css">

  <script src="js/jquery.js"></script>
  <script src="js/jquery-migrate-1.2.1.js"></script>

  <style media="screen">

  </style>
  <!--[if lt IE 9]>
  <html class="lt-ie9">
  <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="Vous utilisez un navigateur internet obsolète (Internet Explorer). Pour une meilleure expérience, téléchargez une navigateur récent de type Chrome, Firefox, Edge, Opéra ou Safari"></a></div>
  </html>
  <script src="js/html5shiv.js"></script><![endif]-->
  <script src="js/device.min.js"></script>
</head>

<body>
  <div class="page">
    <!--
    ========================================================
    HEADER
    ========================================================


  -->
  <header>
    <?php $nav_en_cours  = 'contacts' ?>
    <?php   require('inc/menuheader.php') ?>
  </header>
  <!--
  ========================================================
  CONTENT
  ========================================================
-->
<main>
  <?php
  if (isset($_POST) && isset($_POST['nom']) && isset($_POST['telephone']) && isset($_POST['email']) && isset($_POST['message']))
  {
    include("inc/checkform.php");
  }
  ?>
  <section class="well3 bg-secondary2">
    <div class="container">
      <?php $_SESSION['panier']->getStep(4); ?>
      <div class="row">

        <div class="grid_8">
          <h2 id="myform">Information</h2>

          <form id="addrForm" method="post" action="backend/adress.php" class="mailform off2">
            <center>
              <input  type="radio" name="statut" id="part_radio" value="part">Particulier
              <input type="radio" name="statut" id="pro_radio" value="pro">Professionnel<br />
              <div class="errorMessage"></div>
            </center>
            <fieldset class="row">
              <div id="pro_form" class="fail">
                <label class="grid_4" for="">
                  <div class="inpErr grid_4"><div class="arrow"></div><h7></h7></div>
                  <input type="text"  placeholder="Votre Statut"  name="statutEntp">
                </label>
                <label class="grid_4" for="">
                  <div class="inpErr grid_4"><div class="arrow"></div><h7></h7></div>
                  <input type="text"   placeholder="Nom d'Entreprise"  name="nameEntp">
                </label>
                <label class="grid_4" for="">
                  <div class="inpErr grid_4"><div class="arrow"></div><h7></h7></div>
                  <input type="text" id="numberJU" placeholder="Numéro Siret"  name="siret">
                </label>
              </div>
              <label class="grid_4" for="name">

                <input type="text"  class="inputValidation" placeholder="Votre Nom"  name="surname">
              </label>
              <label class="grid_4" for="name">
                <div class="inpErr grid_4">
                  <div class="arrow"></div>
                  <h7></h7>
                </div>
                <input type="text"  class="inputValidation" placeholder="Votre PRENOM : *"  name="name"  >
              </label>
              <label class="grid_4" for="">
                <div class="inpErr grid_4"><div class="arrow"></div><h7></h7></div>
                <input type="text"  class="inputValidation" id="numberJU" placeholder="Numéro de rue" name="numRoad" maxlength="5" >
              </label>
              <label class="grid_4" for="">
              <div class="inpErr grid_4"><div class="arrow"></div><h7></h7></div>              
                <input type="text"  class="inputValidation" placeholder="Adresse" name="addr">
              </label>
              <label class="grid_4" for="">
                <input type="text"  placeholder="Complément d'Adresse" name="addrComp">
              </label>
              <label class="grid_4" for="">
                <input type="text"  value="<?php echo $panier->getArea()->getCP(); ?>" name="cp" readonly>
              </label>
              <label class="grid_4" for="">
                <input type="text"  value="<?php echo $panier->getArea()->getName(); ?>" name="ville" readonly>
              </label>
              <label class="grid_4" for="">
                <div class="inpErr grid_4"><div class="arrow"></div><h7></h7></div>
                <input type="text"  class="inputValidation" placeholder="Adresse e-mail" name="mail">
              </label>
              <label class="grid_4" for="">
                <div class="inpErr grid_4"><div class="arrow"></div><h7></h7></div>
                <input type="text"  maxlength="10" id="numberJU" class="inputValidation" placeholder="Téléphone" name="tel">
              </label>
              <div id="part_form" class="fail">
                <select class="grid_4" name="locataire" class="inputValidation" id="select">
                  <option value="1" selected>Locataire</option>
                  <option value="0">Propriétaire</option>
                </select>
              </div>
            </fieldset>
            <br>

            <h2 id="myform">Facturation

              <div style="font-size:18px;margin-top:3px;">
                Adresse de facturation identique
                <input id="sameAddress" type="checkbox" class="inputValidation" placeholder="Numéro d'Adresse" name="buttonNotUsed">
              </div>
            </h2>
            <fieldset class="row">
              <label class="grid_4" for="">
                <div class="inpErr grid_4">
                  <div class="arrow"></div>
                  <h7></h7>
                </div>
                <input type="text" class="inputValidation"  id="numberJU" placeholder="Numéro de rue" name="fNumRoad">
              </label>
              <label class="grid_4" for="">
                <div class="inpErr grid_4">
                  <div class="arrow"></div>
                  <h7></h7>
                </div>
                <input type="text"  class="inputValidation" placeholder="Adresse" name="fAddr">
              </label>
              <label class="grid_4" for="">
                <input type="text"  placeholder="Adresse Complémentaire" name="fAddrComp">
              </label>
              <label class="grid_4" for="">
                <div class="inpErr grid_4">
                  <div class="arrow"></div>
                  <h7></h7>
                </div>
                <input type="text" pattern="[0-9]{5}" class="inputValidation"  maxlength="5" id="numberJU" placeholder="Code Postal" value="" name="fCp">
              </label>
              <label class="grid_4" for="">
                <div class="inpErr grid_4">
                  <div class="arrow"></div>
                  <h7></h7>
                </div>
                <input type="text" class="inputValidation" placeholder="Ville" value="" name="fVille">
              </label>
              <div class="grid_4">
                <button type="submit" class="btn">Envoyer</button>
              </div>
            </fieldset>
          </form>
        </div>
        <div class="grid_3 ">
          <section class="contact">
            <div class=" f">
              <div class="l"><img src="images/contact-canaclean.jpg" alt=" - Contact"></div>
              <div class=" r">
                <div>
                  <p>Vous souhaitez une <b>intervention d'urgence</b> pour le débouchage d'une canalisation ?</p>
                  <div>
                    <p>CONTACTEZ-NOUS AU</p><a href="tel:"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                  </div>
                </div>
              </div>
              <div class="s">

                <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                <div class="r t-a-center"><a href="./contacts.php" class="btn btn-block ">NOUS CONTACTER</a></div>
              </div>
            </div>
          </section>
        </div>
      </div>
  </section>

  <div class="container">
    <ul class="row product-list">
      <li class="grid_6">
        <div class="box wow fadeInRight">
          <div class="box_aside">
            <div class="icon fa-comments"></div>
          </div>
          <div class="box_cnt__no-flow">
            <h3><a href="#">A votre service</a></h3>
            <p>Réflechir avant d'agir. Vos problème de wc bouché, de canalisation colmatée, de fuite d'eau doivent être
              solutionnés au plus vite, mais pas n'importe comment et pas à n'importe quel prix!!!</p>
            </div>
          </div>
          <hr>
          <div data-wow-delay="0.2s" class="box wow fadeInRight">
            <div class="box_aside">
              <div class="icon fa-calendar-o"></div>
            </div>
            <div class="box_cnt__no-flow">
              <h3><a href="#">Sur rendez-vous ou en urgence</a></h3>
              <p>Plannifiez un hydrocurage des canalisations, un détartrage de votre tuyauterie, ou appellez nous en urgence
                pour une recherche de fuite, tout est possible, nous intervenons tous les jours de la semaine sur Bordeaux
                Métrople et dans toute la Gironde</p>
              </div>
            </div>
          </li>
          <li class="grid_6">
            <div data-wow-delay="0.3s" class="box wow fadeInRight">
              <div class="box_aside">
                <div class="icon fa-group"></div>
              </div>
              <div class="box_cnt__no-flow">
                <h3><a href="#">Une équipe de professionnels</a></h3>
                <p>Une véritable équipe de professionnels ayant plusieurs années d'expérience ça compte énormément. Chaque
                  chantier est unique, mais l'expérience ça aide toujours.</p>
                </div>
              </div>
              <hr>
              <div data-wow-delay="0.4s" class="box wow fadeInRight">
                <div class="box_aside">
                  <div class="icon fa-thumbs-up"></div>
                </div>
                <div class="box_cnt__no-flow">
                  <h3><a href="#">Service de qualité</a></h3>
                  <p>Qualité et efficacité font partie de l'adn de débouchage-bordeaux.fr. Pas de surprise, nous intervenons
                    pour déboucher vos toilettes, hydrocurer les canalisations ou rechercher une fuite. Et le tarif, nous
                    l'annonçons au début de l'intervention, pas à la fin.</p>
                  </div>
                </div>
              </li>
            </ul>
            <br>
          </div>
        </div>
      </section>


    </main>
    <!--
    ========================================================
    FOOTER
    ========================================================
  -->
  <footer>
    <?php require('inc/menufooter.php'); ?>
  </footer>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/step4.js"></script>
</body>
<script type="text/javascript">
$(document).ready(function(){
     $('[id^=numberJU]').keypress(validateNumber);
  $('#sameAddress').click(function(e){
    if ($(this).prop('checked')) {
      $('input[name="fCp"]').val($('input[name="cp"]').val());
      $('input[name="fNumRoad"]').val($('input[name="numRoad"]').val());
      $('input[name="fAddr"]').val($('input[name="addr"]').val());
      $('input[name="fAddrComp"]').val($('input[name="addrComp"]').val());
      $('input[name="fVille"]').val($('input[name="ville"]').val());
    }
  });
});
$(document).keypress(function(e){
  setTimeout(function(){
    if ($('#sameAddress').prop('checked')) {
      $('input[name="fCp"]').val($('input[name="cp"]').val());
      $('input[name="fNumRoad"]').val($('input[name="numRoad"]').val());
      $('input[name="fAddr"]').val($('input[name="addr"]').val());
      $('input[name="fAddrComp"]').val($('input[name="addrComp"]').val());
      $('input[name="fVille"]').val($('input[name="ville"]').val());
    }
  }, 200);
});


function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46 || event.keyCode == 9) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
    	return true;
    }
};

</script>
</html>
