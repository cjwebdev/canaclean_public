<?php
	require_once "lib/html2pdf.php";
	include_once('php/global.php');
	include_once('pdf/fpdf.php');
	include_once($INCLUDE . 'Product.inc.php');
	include_once($INCLUDE . 'Panier.inc.php');
	include_once($INCLUDE . 'Area.inc.php');
	include_once($INCLUDE . 'Option.inc.php');
	session_start();
	ob_start();
	$panier = $_SESSION['panier'];
	$session = $_SESSION['panier'];
  $price = $panier->getProduct()->getPriceHT($panier->getArea()->getNumZone());
  $prix_eligible = ($price + ($panier->getArea()->getMajor() / 100) * $price);
  $addrF = "";
$pro = "";

?>

<html lang="fr">
<style>
@font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

#center{
  margin-left:auto;
  margin-right:auto;
  width:50%
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 98%;
  height: 29.7cm;
  margin: 0 auto;
  color: #555555;
  background: #FFFFFF;
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-family: SourceSansPro;
}

#main {
  position: relative;
  width: 97%;
  padding: 5px;
}

#header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

#logo img {
  height: 70px;
}

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #0087C3;
  float: left;
}

#client .to {
  color: #777777;
}

h2.name {
  font-size: 1.4em;
  font-weight: normal;
  margin: 0;
}

#invoice {
margin-top: 5px
  text-align: left;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  font-weight: normal;
  margin-bottom: 15px;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table th,
table td {
  padding: 49px;
  padding-top: 49px;
  padding-bottom: 49px;
  background: #EEEEEE;
  text-align: center;
  border-bottom: 1px solid #FFFFFF;
  padding-top: 32px;
  padding-bottom: 32px;
  font-size: 15px;

}

table th {
  white-space: nowrap;
  font-weight: normal;
}

table td {
  text-align: right;
}

table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1.6em;
  background: #57B223;
}

table .desc {
  text-align: left;
  width: 70px;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 10px 20px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1.2em;
  white-space: nowrap;
  border-top: 1px solid #AAAAAA;
}

table tfoot tr:first-child td {
  border-top: none;
}

table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1.4em;
  border-top: 1px solid #57B223;

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  width:90%;
  border-left: 6px solid #0087C3;
}

#notices .notice {
  font-size: 1.2em;
}

#footer {
  color: #777777;
  width: 90%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

</style>
<body>
  <div id="header" class="clearfix">
    <div id="logo">
      <img src="images/logo_facture.jpg">
    </div>
    <div id="company">
      <h2 class="name">AG-Assainissement</h2>
      <div>31 rue Aristide Berges</div>
      <div>33270 FLOIRAC</div>
      <div>05 56 88 65 66</div>
    </div>
</div>
<div id="main">
  <div id="details" class="clearfix">
    <div id="client">
      <div class="to">Adresse de facturation:</div>
      <div class="name"><?php echo " ".$pro." ".$session->getAddress()->getName() ." ". $session->getAddress()->getSurName().""; ?></div>
      <div class="address"><?php echo " ". $session->getAddress()->getFNumRoad()." ". $session->getAddress()->getFAddr()."".$addrF.""; ?></div>
      <div class="email"><?php echo "". $session->getAddress()->getFCP()." " . $session->getAddress()->getFVille(). " "; ?> </div>
    </div>
    <div id="invoice">
      <h1>Devis</h1>
      <div class="date">Date de facturation: <?php echo date("d.m.y"); ?></div>
      <div class="date">Devis n: <?php echo commandNextId($bdd); ?></div>
    </div>
  </div>
  <div id="center">
  <table border="0" cellspacing="0" cellpadding="0" style="">
    <thead>
      <tr>
        <th class="no">#</th>
        <th class="desc">PRESTATION</th>
        <th class="qty">QUANTITER</th>
        <th class="total">TOTAL</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="no">01</td>
        <td class="desc">
          <h3><?php echo $panier->getProduct()->getName(); ?></h3>
        </td>
            <td class="qty">1</td>
            <td class="total"><?php echo $panier->getTotalHT(); ?> €</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td colspan="1">TOTAL HT</td>
            <td>  <?php echo $panier->getTotalHT(); ?>€</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="1">TVA</td>
            <td><?php echo $panier->getTVA(); ?>%</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="1">TOTAL</td>
            <td><?php echo $panier->getTotalTTC(); ?>€</td>
          </tr>
        </tfoot>
      </table>
      </div>
      <div id="thanks">Thank you!</div>
      <div id="notices">
        <div>Condition:</div>
        <div class="notice">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
      </div>
  </div>
  </body>
  </html>


<?php
	$content = ob_get_clean();
	try {
		$pdf = new HTML2PDF("p","A4","fr");
		$pdf->pdf->SetAuthor('AG');
		$pdf->pdf->SetTitle('Devis');
		$pdf->pdf->SetSubject('Devis');
		$pdf->pdf->SetKeywords('Devis');
		$pdf->writeHTML($content);
		$pdf->Output('devis.pdf');
		// $pdf->Output('Devis.pdf', 'D');
	} catch (HTML2PDF_exception $e) {
		die($e);
	}

?>
