<?php
include_once('php/global.php');
include_once($INCLUDE . 'Panier.inc.php');
include_once($INCLUDE . 'Product.inc.php');
include_once($INCLUDE . 'Panier.inc.php');
include_once($INCLUDE . 'Option.inc.php');
session_start();
//Check previous step of procedure
function noOptions($opts){
  $ret = false;
  for ($i=0; $i < 10; $i++) {
    if (count($opts[$i]) > 0){
      $ret = true;
    }
  }
  return $ret;
}
if (!isset($_POST['id']))
{
  header('Location: step2.php?error');
  exit;
}

//set product
include_once('php/step3.php');
/*
if (!noOptions($optsByLevel))
{
header('Location: step4.php?error');
exit;
}
*/

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <title>Tarifs Débouchage / Vidange / Fuite</title>
  <meta charset="utf-8">
  <meta name="description" content="Prix et tarifs Débouchage WC, débouchage canalisations">
  <?php require('inc/meta.php'); ?>
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/grid.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/tarifs.css">
  <link rel="stylesheet" href="css/step3.css">

  <script src="js/jquery.js"></script>
  <script src="js/jquery-migrate-1.2.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
  <!--[if lt IE 9]>
  <html class="lt-ie9">
  <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="../images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
  </html>
  <script src="../js/html5shiv.js"></script><![endif]-->
  <script>
    $(document).ready(function () {
      $("input[type='radio']").click(function () {
        var previousValue = $(this).attr('previousValue');
        var name = $(this).attr('name');

        if (previousValue == 'checked') {
          $(this).removeAttr('checked');
          $(this).attr('previousValue', false);
        }
        else {
          $("input[name=" + name + "]:radio").attr('previousValue', false);
          $(this).attr('previousValue', 'checked');
        }
      });
    });
  </script>
  <style>

  </style>

</head>
<body>
  <div class="page">
    <!--
    ========================================================
    HEADER
    ========================================================
  -->
  <header>
    <?php require('inc/menuheader.php'); ?>
  </header>
  <!--
  ========================================================
  CONTENT
  ========================================================
-->
<style>
  .section_bloc {
    padding: 29px;
    padding-top: 29px;
    padding-bottom: 29px;
    background-color: #f6f6f6;
    padding-top: 15px;
    padding-bottom: 15px;
    margin-bottom: 17px;
  }

</style>
<main id="print">
  <form method="post" action="backend/options.php">

    <section class="well1 ins2 mobile-center">
      <div class="container">
        <?php $_SESSION['panier']->getStep(3); ?>
        <br />
        <div style="background-color:#f9f9f9;margin-bottom: 20px;padding: 30px;">
          <section style="padding-left:70px;text-align:center;" class="tarif">
            <div class="row">
              <h2>
                <?php echo $_SESSION['panier']->getProduct()->getName();
                ?>
                <!--
                <span>
                <?php
                echo $pdt->getPriceHT($_SESSION['panier']->getArea()->getNumZone() );
                ?>
                € HT
              </span>
            -->
          </h2>
          <h6>
            <?php echo $_SESSION['panier']->getProduct()->getDesc(); ?>
          </h6>
        </div>
      </section>
    </div>

    <div class="row ">

      <div class="col-sm-8 col-lg-9">
        <section class="tarifs">
          <section class="prestation">
            <section class="tarif-header">
              <div class="row">
                <div class="separator"></div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8"></div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prix"></div>
              </div>
            </section>
            <section class="tarif">
                        <div class="row">
                          <div class="separator"></div>
                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 tarif-denom">
                            <a>
                              <h3 class="tarif-titre">
                              <?php echo $_SESSION['panier']->getProduct()->getName(); ?>
                              </h3>
                            </a>
                            <p class="tarif-titre pointer" style="font-size:12px">
                              <div class="info" >
                              <?php echo $_SESSION['panier']->getProduct()->getDesc(); ?>                              </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prixht" style="font-size:25px">

                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prix">
                              <input required type="radio" name="o" value="" checked>
                            </div>
                          </div>
                        </section>
            <?php for ($i=1; $i < 10; $i++) {
              $done = true
              ?>
              <?php if (count($optsByLevel[$i - 1]) > 0 && $optsByLevel[$i - 1][0]->getRequired()): ?>
                <div style="background-color:#f9f9f9;margin-bottom: 20px;padding: 30px;">
                <?php endif; ?>
                <?php foreach ($optsByLevel[$i - 1] as $opt): ?>
                  <?php if ($i == $opt->getLevel()) { ?>
                    <?php if ($opt->getRequired()) { $done = true;?>

                      <section class="tarif">
                        <div class="row">
                          <div class="separator"></div>
                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 tarif-denom">
                            <a>
                              <h3 class="tarif-titre">
                                <?php echo $opt->getName(); ?>
                              </h3>
                            </a>
                            <p class="tarif-titre pointer" style="font-size:12px">
                              <div class="info" >
                                <?php echo $opt->getDesc(); ?>
                              </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prixht" style="font-size:25px">
                              <?php
                              if($opt->getId() == "3" || $opt->getId() == "4"){
                              ?>
      
                             <?php } else { ?>
                              <?php echo $pdt->getPricePlusOption($_SESSION['panier']->getArea()->getNumZone(), $opt); ?> €
                            <?php } ?>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prix">
                              <input required type="radio" name="option<?php echo $i; ?>" value="<?php echo $opt->getId(); ?>">
                            </div>
                          </div>
                        </section>
                        <?php } ?>
                        <?php } ?>

                      <?php endforeach; ?>
                      <?php if (count($optsByLevel[$i - 1]) > 0 && $optsByLevel[$i - 1][0]->getRequired()): ?>
                      </div>
                    <?php endif; ?>

                    <?php } ?>

                    <!-- Calc -->
                    <?php
                    $haveoptionnal = false;
                    for ($i=1; $i < 10; $i++) {
                      if (count($optsByLevel[$i - 1]) > 0 && !$optsByLevel[$i - 1][0]->getRequired())
                      {
                        ?> <h2>OPTIONNELLE</h2><?php
                        break;
                      }
                    }
                    ?>

                    <?php for ($i=1; $i < 10; $i++) {
                      $done = true
                      ?>
                      <?php if (count($optsByLevel[$i - 1]) > 0 && !$optsByLevel[$i - 1][0]->getRequired()): ?>
                        <div style="background-color:#f9f9f9;margin-bottom: 20px;padding: 30px;">
                        <?php endif; ?>
                        <?php foreach ($optsByLevel[$i - 1] as $opt): ?>
                          <?php if ($i == $opt->getLevel()) { ?>
                            <?php if (!$opt->getRequired()) { $done = true;?>

                              <section class="tarif">
                                <div class="row">
                                  <div class="separator"></div>
                                  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 tarif-denom">
                                    <a>
                                      <h3 class="tarif-titre">
                                        <?php echo $opt->getName(); ?>
                                      </h3>
                                    </a>
                                    <p class="tarif-titre pointer" style="font-size:12px">
                                      <div class="info" >
                                        <?php echo $opt->getDesc(); ?>
                                      </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prixht" style="font-size:25px">
                                      + <?php echo $opt->getMajor();?> €
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 tarif-prix">
                                      <input type="radio" name="option<?php echo $i; ?>" value="<?php echo $opt->getId(); ?>">
                                    </div>
                                  </div>
                                </section>
                                <?php } ?>
                                <?php } ?>

                              <?php endforeach; ?>
                              <?php if (count($optsByLevel[$i - 1]) > 0 && !$optsByLevel[$i - 1][0]->getRequired()): ?>
                              </div>
                            <?php endif; ?>
                            <?php } ?>
                            <button type="submit" class="bouton_suivant" value="">Suivant</button>
                          </section>
                        </section>
                        <div id="editor"></div>
                      </form>
                    </div>
                    <div class="col-sm-4 col-lg-3">
                      <section class="contact">
                        <div class=" f">
                          <div class="l"><img src="images/contacter-canaclean.jpg" alt=" - Contact" class="img-responsive"></div>
                          <div class=" r">
                            <div>
                              <p>Vous souhaitez une intervention d'urgence pour le débouchage d'une canalisation</p>
                              <div>
                                <p>CONTACTEZ-NOUS AU</p><a href="tel:" class="btn btn-block"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                              </div>
                            </div>
                          </div>
                          <div class="s">
                            <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                            <div class="r t-a-center"><a href="../contacts.php" class="btn btn-block"><i class="fa-comments"></i> NOUS CONTACTER </a></div>
                          </div>
                        </section>
                      </div>
                    </div>
                    <hr>
                    <div class="container">
                      <ul class="row product-list">
                        <li class="grid_6">
                          <div class="box wow fadeInRight">
                            <div class="box_aside">
                              <div class="icon fa-comments"></div>
                            </div>
                            <div class="box_cnt__no-flow">
                              <h3><a href="#">A votre service</a></h3>
                              <p>Réflechir avant d'agir. Vos problème de wc bouché, de canalisation colmatée, de fuite d'eau doivent être
                                solutionnés au plus vite, mais pas n'importe comment et pas à n'importe quel prix!!!</p>
                              </div>
                            </div>
                            <hr>
                            <div data-wow-delay="0.2s" class="box wow fadeInRight">
                              <div class="box_aside">
                                <div class="icon fa-calendar-o"></div>
                              </div>
                              <div class="box_cnt__no-flow">
                                <h3><a href="#">Sur rendez-vous ou en urgence</a></h3>
                                <p>Plannifiez un hydrocurage des canalisations, un détartrage de votre tuyauterie, ou appellez nous en urgence
                                  pour une recherche de fuite, tout est possible, nous intervenons tous les jours de la semaine sur Bordeaux
                                  Métrople et dans toute la Gironde</p>
                                </div>
                              </div>
                            </li>
                            <li class="grid_6">
                              <div data-wow-delay="0.3s" class="box wow fadeInRight">
                                <div class="box_aside">
                                  <div class="icon fa-group"></div>
                                </div>
                                <div class="box_cnt__no-flow">
                                  <h3><a href="#">Une équipe de professionnels</a></h3>
                                  <p>Une véritable équipe de professionnels ayant plusieurs années d'expérience ça compte énormément. Chaque
                                    chantier est unique, mais l'expérience ça aide toujours.</p>
                                  </div>
                                </div>
                                <hr>
                                <div data-wow-delay="0.4s" class="box wow fadeInRight">
                                  <div class="box_aside">
                                    <div class="icon fa-thumbs-up"></div>
                                  </div>
                                  <div class="box_cnt__no-flow">
                                    <h3><a href="#">Service de qualité</a></h3>
                                    <p>Qualité et efficacité font partie de l'adn de débouchage-bordeaux.fr. Pas de surprise, nous intervenons
                                      pour déboucher vos toilettes, hydrocurer les canalisations ou rechercher une fuite. Et le tarif, nous
                                      l'annonçons au début de l'intervention, pas à la fin.</p>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </section>

                      </main>
                      <!--
                      ========================================================
                      FOOTER
                      ========================================================
                    -->
                    <footer>
                      <?php require('inc/menufooter.php'); ?>

                    </footer>
                  </div>
                  <script src="js/script-inner.js"></script>
                  <script>
                    (function (i, s, o, g, r, a, m) {
                      i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                      }, i[r].l = 1 * new Date(); a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-70918441-6', 'auto');
                    ga('send', 'pageview');
                  </script>
                </body>

                </html>
