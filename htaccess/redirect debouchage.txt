		<ul class="nav">
					<li class="tab"><a href="/" title="Accueil" class="">Accueil</a></li>
					<li class="tab"><a href="/assainissement-pessac" title="Assainissement " class="">Assainissement </a></li>
					<li class="tab"><a href="/fosses-septiques-pessac" title="Fosse septique - Fosse toutes eaux" class="">Fosse septique - Fosse toutes eaux</a></li>
					<li class="tab"><a href="/vidange-curage-pessac" title="Pompage & Vidange Fosse Septique" class="">Pompage & Vidange Fosse Septique</a></li>
					<li class="tab"><a href="/pompesrelevage-pessac" title="Station & Pompes de relevage des eaux" class="">Station & Pompes de relevage des eaux</a></li>
		</ul>
		<ul class="nav">
					<li class="tab"><a href="/bacagraisse" title="Bac à Graisse" class="">Bac à Graisse</a></li>
					<li class="tab"><a href="/traitement-des-eaux-materiel-pessac" title="Traitement des eaux usées" class="">Traitement des eaux usées</a></li>
					<li class="tab"><a href="/pose-entretien-canalisations-pessac" title="Canalisations - VRD" class="">Canalisations - VRD</a></li>
					<li class="tab"><a href="/detection-fuite-eau" title="Détection de fuite" class="">Détection de fuite</a></li>
					<li class="tab"><a href="/inspection-videocamera-canalisations" title="Inspection Vidéo caméra Canalisations" class="">Inspection Vidéo caméra Canalisations</a></li>
		</ul>
		<ul class="nav">
					<li class="tab"><a href="/travauxdrainage-pessac" title="Drainage - épandage des eaux usées" class="">Drainage - épandage des eaux usées</a></li>
					<li class="tab"><a href="/hydrocurage-canalisations" title="Hydrocurage Canalisations" class="">Hydrocurage Canalisations</a></li>
					<li class="tab"><a href="/terrassement-pessac" title="Terrassement Viabilisation" class="">Terrassement Viabilisation</a></li>
					<li class="tab"><a href="/debouchagecanalisations-pessac" title="Débouchage" class="">Débouchage</a></li>
					<li class="tab"><a href="/contact" title="Contact" class="">Contact</a></li>
		</ul>