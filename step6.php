<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include_once('php/global.php');
include_once('pdf/fpdf.php');
include_once($INCLUDE . 'Product.inc.php');
include_once($INCLUDE . 'Panier.inc.php');
include_once($INCLUDE . 'Area.inc.php');
include_once($INCLUDE . 'Option.inc.php');
session_start();
if(!$_POST['vads_trans_status']){
   header('Location: step5.php');
};

$panier = $_SESSION['panier'];
$prix_eligible = ($panier->getProduct()->getPriceHT() + ($panier->getArea()->getMajor() / 100) * $panier->getProduct()->getPriceHT());


?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Contacts</title>
    <meta charset="utf-8">
    <meta name="description" content="Restons en contact au 05 33 08 08 08 ! Canaclean vous assure une intervention  pour le débouchage wc, débouchage canalisations à Bordeaux et sur l'ensemble de la Gironde du lundi au dimanche sur simple appel téléphonique">
    <meta name="keyword" content="contact débouchage arcachon, contact débouchage wc arcachon, contact pour déboucher wc arcachon, débouche wc arcachon ">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <?php   require('inc/meta.php') ?>
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/google-map.css">
    <link rel="stylesheet" href="css/mailform.css">
    <link rel="stylesheet" href="css/tarifs.css">
    <link rel="stylesheet" href="css/step.css">
    <link rel="stylesheet" href="css/pdf.css">

    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="Vous utilisez un navigateur internet obsolète (Internet Explorer). Pour une meilleure expérience, téléchargez une navigateur récent de type Chrome, Firefox, Edge, Opéra ou Safari"></a></div>
    </html>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/PayzenJS@1.0.5/payzenjs.js"></script>
</head>

<body>
	  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=905277863011455";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <div class="page">
        <!--
        ========================================================
        HEADER
        ========================================================


    -->
    <header>
        <?php   require('inc/menuheader.php'); ?>
    </header>
    <!--
    ========================================================
    CONTENT
    ========================================================
-->
<main>
    <?php
    if (isset($_POST) && isset($_POST['nom']) && isset($_POST['telephone']) && isset($_POST['email']) && isset($_POST['message']))
    {
        include("inc/checkform.php");
    }
    ?>
    <section class="well3 bg-secondary2">
        <div class="container">
            <div class="row">

                <div class="grid_8">
                    <h2>Récapitulatif de commande</h2><br>
                    <table border="0" cellspacing="0" cellpadding="0" style="">
                      <thead>
                        <tr>
                          <th class="no">#</th>
                          <th class="desc">PRESTATION</th>
                          <th class="qty">QUANTITER</th>
                          <th class="total">TOTAL</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="no">01</td>
                          <td class="desc">
                            <h3><?php echo $panier->getProduct()->getName(); ?></h3>
                            <?php
                            foreach($panier->getOptions() as $opt) {
                              ?>
                              <i><?php echo $opt->getName(); ?></i><br />
                              <?php } ?></td>
                              <td class="qty">1</td>
                              <td class="total"><?php echo $panier->getTotalZoneOptionsHT(); ?></td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td colspan="2"></td>
                              <td colspan="1">TOTAL HT</td>
                              <td>  <?php echo $panier->getTotalHT(); ?>€</td>
                            </tr>
                            <tr>
                              <td colspan="2"></td>
                              <td colspan="1">TVA</td>
                              <td><?php echo $panier->getTVA(); ?>%</td>
                            </tr>
                            <tr>
                              <td colspan="2"></td>
                              <td colspan="1">TOTAL</td>
                              <td><?php echo $panier->getTotalTTC(); ?>€</td>
                            </tr>
                          </tfoot>
                        </table>
                        <div style="float:left;width:35%;margin-left:15px;margin-top:-105px">
                            <?php 
                                if($_POST['vads_trans_status'] == "AUTHORISED"){
                            ?>
                                    <a href="devis.php" target="_blank" class="bouton">DEVIS PDF</a>
                                    <input type="submit" click="paiement()" class="bouton" id="paiement" name="send_paiement" value="PAIEMENT" style="height:40px;" />
                            <?php } else { ?>
                                      <input type="submit" click="paiement()" class="bouton" id="paiement" name="send_paiement" value="ECHEC DE PAIEMENT" style="height:40px;" />
                            <?php  } ?>
                                </div>
                            </div>
                            <div class="grid_3 ">
                                <section class="contact">
                                    <div class=" f">
                                        <div class="l"><img src="images/contact-canaclean.jpg" alt=" - Contact"></div>
                                        <div class=" r">
                                            <div>
                                                <p>Vous souhaitez une <b>intervention d'urgence</b> pour le débouchage d'une canalisation ?</p>
                                                <div>
                                                    <p>CONTACTEZ-NOUS AU</p><a href="tel:"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="s">
                                            <!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> -->
                                            <div class="r t-a-center"><a href="./contacts.php" class="btn btn-block ">NOUS CONTACTER</a></div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
                    <br>
                    <div class="container">
                        <ul class="row product-list">
                            <li class="grid_6">
                                <div class="box wow fadeInRight">
                                    <div class="box_aside">
                                        <div class="icon fa-comments"></div>
                                    </div>
                                    <div class="box_cnt__no-flow">
                                        <h3><a href="#">A votre service</a></h3>
                                        <p>Réflechir avant d'agir. Vos problème de wc bouché, de canalisation colmatée, de fuite d'eau doivent être
                                            solutionnés au plus vite, mais pas n'importe comment et pas à n'importe quel prix!!!</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div data-wow-delay="0.2s" class="box wow fadeInRight">
                                        <div class="box_aside">
                                            <div class="icon fa-calendar-o"></div>
                                        </div>
                                        <div class="box_cnt__no-flow">
                                            <h3><a href="#">Sur rendez-vous ou en urgence</a></h3>
                                            <p>Plannifiez un hydrocurage des canalisations, un détartrage de votre tuyauterie, ou appellez nous en urgence
                                                pour une recherche de fuite, tout est possible, nous intervenons tous les jours de la semaine sur Bordeaux
                                                Métrople et dans toute la Gironde</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="grid_6">
                                        <div data-wow-delay="0.3s" class="box wow fadeInRight">
                                            <div class="box_aside">
                                                <div class="icon fa-group"></div>
                                            </div>
                                            <div class="box_cnt__no-flow">
                                                <h3><a href="#">Une équipe de professionnels</a></h3>
                                                <p>Une véritable équipe de professionnels ayant plusieurs années d'expérience ça compte énormément. Chaque
                                                    chantier est unique, mais l'expérience ça aide toujours.</p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div data-wow-delay="0.4s" class="box wow fadeInRight">
                                                <div class="box_aside">
                                                    <div class="icon fa-thumbs-up"></div>
                                                </div>
                                                <div class="box_cnt__no-flow">
                                                    <h3><a href="#">Service de qualité</a></h3>
                                                    <p>Qualité et efficacité font partie de l'adn de débouchage-bordeaux.fr. Pas de surprise, nous intervenons
                                                        pour déboucher vos toilettes, hydrocurer les canalisations ou rechercher une fuite. Et le tarif, nous
                                                        l'annonçons au début de l'intervention, pas à la fin.</p>
                                                    </div>
                                                </div>
                                                <br>
                                            </li>

                                        </ul>
                                    </div>

                        </main>
                        <!--
                        ========================================================
                        FOOTER
                        ========================================================
                    -->
                    <footer>
                        <?php require('inc/menufooter.php'); ?>
                    </footer>
                </div>

                <script src="http://code.jquery.com/jquery-latest.min.js"></script>
            </body>

            </html>
