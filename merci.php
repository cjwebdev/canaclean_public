<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Merci</title>
    <meta charset="utf-8">
	<meta name="description" content="Commande validée. Débouchage WC par pompe manuelle ou hydrocurage selon l'importance et la localisation du bouchon dans la canalisation wc.">
	<?php   require('inc/meta.php') ?> 	
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">	
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/tarifs.css">
    <link rel="stylesheet" href="css/mailform.css">	
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
      <div style="clear: both; text-align:center; position: relative;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    </html>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="js/device.min.js"></script>
  </head>
  <body>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=905277863011455";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <div class="page">
		<!--
      ========================================================
      							HEADER
      ========================================================
      -->
      <header>
          <?php require('inc/menuheader-inner.php')?> 
      </header>
      <!--
      ========================================================
                                  CONTENT
      ========================================================
      -->
      <main>
        <section class="well1 ins4 bg-image-merci">
			<div class="container">
            <div class="row">
              <div class="grid_7 preffix_5">
                <h2>Paiement validé<br/>Merci pour votre commande</h2>
                <p>Nous allons à présent entrez en contact pour valider ensemble la date d'intervention et son créneau horaire. Pour cela, entrez votre numéro de tel dans le champs ci dessous pour être directement mis en relation avec notre planning. </p>
				<h3>Plannifiez votre intervention en direct</h3>
				<form method="post" action="clic2call.php"  class="mailform off2">				
					<fieldset class="row">
						<!-- <label class="grid_3" for="name">
						<input type="text"  placeholder="Votre NOM : *"  name="nom"  >
						</label> -->
						<label class="grid_3" for="telephone">
						<input type="text"  placeholder="Telephone : *" name="telephone" >
						</label>
						<div class="grid_3">
						<button type="submit" class="btn" ><i class="fa-phone"></i> Mise en relation</button>
						</div>				
					</fieldset>
				</form>
                <!--<div class="row off4">
                  <div class="grid_3">
                    <ul class="mymarked-list wow fadeInRight">
                      <li>WC bouché</li>
                      <li>Toilettes bouchées</li>					  
                      <li>Sanibroyeur bouché</li>
                      <li>Sanibroyeur bloqué</li>
                    </ul>
                  </div>
                  <div class="grid_3">
                    <ul data-wow-delay="0.2s" class="mymarked-list wow fadeInRight">
                      <li>Débouchage Haute pression</li>
                      <li>Débouchage 100% écologique</li>
                      <li>Débouchage Pompe Manuelle</li>
                      <li>Débouchage Furet</li>
                    </ul>
                  </div>
                </div>-->
              </div>
            </div>
          </div>
        </section>
	<!--  <section class="well1 ins2 mobile-center">
          <div class="container">
            <h2>Comment pour déboucher les wc ? Nos solutions</h2>
            <div class="row off2 HomeBloc">
				<div class="grid_4 HomeBloc">
					<h3>Débouchage wc à la pompe</h3>
					<img src="images/wc-debouchage-pompe-manuelle.jpg" alt="Wc bouchés, débouchage avec une pompe manuelle">            
					<p>Il est possible de déboucher un wc avec une pompe manuelle spéciale ou un furet si tenté que le bouchon ne soit ni trop loin ni trop conséquent. Ces techniques de débouchage éprouvées conviennent dans 50% des cas. Pour le vérifier, contactez-nous</p>
					<a href="contacts.php#myform" class="" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
					<a href="tel:0609151330" class="   fa-phone"> Appel d'Urgence</a>
				</div>
				<div class="grid_4">
					<h3>Débouchage WC Haute Pression</h3>
					<img src="images/debouchage-wc-jet-haute-pression.jpg" id="debouchage-wc-haute-pression" alt="Débouchage wc par Haute pression">
					<p>Pour déboucher vos wc par la technique de haute pression, nos techniciens utilisent un jet propulsant de l'eau sous haute pression afin de disloquer le bouchon qui colmate la canalisation des toilettes</p>
					<a href="contacts.php#myform" class="" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
					<a href="tel:0609151330" class="   fa-phone"> Appel d'Urgence</a>				
				</div>
                <div class="grid_4">
					<h3>Débouchage 100% Écologique</h3>
					<img src="images/debouchage-100pour100-ecologique.png" alt="Mister Service Miami provide unclogging service at your home">
					<p>Le débouchage 100% écologique est réalisé SANS produits nocifs pour l'environnement et votre santé. Ce débouchage n'altère pas les canalisations car il est totalement dépourvu de produits corrosifs. </p>
					<a href="contacts.php#myform" class="" title="Envoyez un message par le formulaire de contact pour prendre rendez-vous avec Débouchage-bordeaux">Prendre Rendez-vous</a>
					<a href="tel:0609151330" class="   fa-phone"> Appel d'Urgence</a>				
				</div> 
            </div>
			<hr>
			<div class="row off2 fadeInRight animated">
				<div class="grid_4">
					<h3> Tarif débouchage wc</h3>
					<p>Pour déboucher les toilettes, Canaclean vous propose 2 tarifs:</p>
					<ul>
						<li class="fa-ellipsis-h"> l'option à bas coût dès 99€ qui consiste à déboucher les toilettes à l'aide d'une pompe manuelle,</li>
						<li class="fa-ellipsis-h" > et l'option all inclusive à partir de 250€ HT pour le débouchage <i><u><strong>et</strong></u></i> l'entretien de la canalisation d'évacuation. </li>
					</ul>
					<table class="wow fadeInUp">
						  <tr>
							<td></td>
							<td>Tarif H.T</td>
						  </tr>
						  <tr>
							<td>Débouchage Manuel <i class="fa fa-ellipsis-h" aria-hidden="true"></i></td>
							<td>99€</td>
						  </tr>
						  <tr>
							<td>Débouchage Haute pression <i class="fa fa-ellipsis-h" aria-hidden="true"></i></td>
							<td>250€</td>
						  </tr>				  

						</table>
					<a href="step1.php" class="btn "><i class="fa-shopping-cart"></i> Commande</a>
				</div>
				<div class="grid_4">
					<h3>Conditions tarifaires débouchage</h3>
					<ul>
						<li>Hors taxes (HT):
						<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> particuliers = +10%,</li>
						<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> professionnels = +20%</li></li>
						<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Notre prestation de "Débouchage manuel" / "débouchage domestique" comprend le débouchage wc,  à l'aide d'une pompe manuelle ou d'un furet. Le débouchage domestique ne comprends pas les prestations de curage et/ou hydrocurage des canalisations.</li>
						<li><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Obligation du client : <br/>
						Lors d’une prestation en hydrocurage (débouchage haute pression) : Mettre à disposition de notre technicien une arrivée d’eau.</li>
					</ul>
				</div>
                 <div class="grid_3">
					<section class="contact">
						<div class=" f">
							<div class="l"><img src="images/contact-canaclean.jpg" alt=" - Contact"></div>
							<div class=" r">
								<div>
									<p>Vous souhaitez une intervention d'urgence pour déboucher les wc?</p>
									<div>
										<p>TÉLÉPHONEZ AU</p><a href="tel:"><i class="fa fa-phone"></i> 06 09 15 13 30</a></div>
								</div>
							</div>
						</div>
						<div class="s">
						<!-- <div class="l t-a-center"><a href="#modal-urgence_debouchage" data-toggle="modal" class="btn btn-block btn-black-orange">DEMANDE DE RAPPEL</a></div> 
							<div class="r t-a-center"><a href="./contacts.php" class="btn btn-block ">NOUS CONTACTER</a></div>
						</div>
					</section>
				</div>
            </div>
          </div>
        </section> --> 
      </main>
<!--
========================================================
                            FOOTER
========================================================
-->
<footer>
  <?php require('inc/menufooter.php')?> 
</footer>
    </div>
    <script src="js/script-inner.js"></script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106590250-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());
  gtag('config', 'UA-106590250-1');
</script>
</body>
</html>